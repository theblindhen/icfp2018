The Blind Hen 2018 ICFP Contest submission
==========================================

Public ID: 0044

Team members
------------

Johan S. H. Rosenkilde
Jonas B. Jensen
Kasper Svendsen

Strategy
--------

Overall, we have built several strategies, we run them all and pick the best one
for the given problem. To ease writing strategies, we designed a medium-level
language above the basic instruction sets which abstracts away the bot-ids, seed
numbers and other details.

Assembly-strategies:

    1. Parallelisation:
        For assembly, we parallelise at the beginning, either into a line or a
        grid of bots, each responsible for a sub-region of the entire model. The
        sub-regions are attempted balanced by volume, principally along the
        x-axis, then along the z-axis.

        Each bot then runs a fairly simple flood-filler with two optimisations: 1.
        it runs quickly over empty areas; and 2. it attempts to fill multiple voxels
        between each move. The flood-filler runs from y=0 and up. It never
        leaves its designated region except at the top thus avoiding any
        collission problems.

    2. Layering:
        We can instantiate our parallelisation strategy to work on thick slices
        of the y-dimension at a time, and synchronising all bots after each such
        "layer". The aim is to minimise the time spent in High energy when the
        model is badly concave. We use a heuristic to select the thickness of
        layers.

    3. Flipping x-z:
        We can flip the x and z dimensions of the model, run any of our strategies,
        and then flip the model back.

Deconstruction-strategy:

    Our medium-level language allows us to invert any trace of instructions: an
    assembly of a model now becomes a disassembly of the same model. This allows
    us to instantiate any of our assembly strategies for disassembly.

Reconstruction-strategy:

    We simply completely remove the existing model and then build the new one.
    This therefore just combines the above two strategies.

Other notes:

    We implemented a simulator for the instruction language in in-memory
    representation (though not a parser for the file format).

    We realised early that the only thing that matters for scoring is the number
    of time steps: all other energy uses are of orders of magnitude lower.

    During Monday, we ran our entire framework regularly on all maps on an Amazon
    Cloud machine to generate the traces.

    Note that the supplied source code is not necessarily able to generate our exact
    submission (though it should be close): Since Saturday morning, we have at all
    times kept the best trace we ever generated using our strategies. Even our
    strategies overall have improved during the contest, it is the nature of such
    computationally hard problems, that generally inferior strategies can perform
    well on special instances.



How to build
------------

First install the "opam" package manager and configure it as follows:

    $ sudo apt-get install opam
    $ opam init # Answer `y` at the prompt
    $ opam switch 4.07.0
    follow instructions after previous command if run

Install packages we need:

    $ sudo apt-get install autoconf
    $ opam install core merlin yojson dune bitv oseq

Now build and run this program:

    $ make
    $ _build/default/app/TODO_ADD_FILE_NAME.exe

On subsequent terminal sessions, do the following unless you allowed `opam init`
to modify your shell rc:

    $ eval `opam config env`

How to run
----------

    $ mkdir traces
    $ make -j2 all_traces  # Increase '2' if you have lots of RAM/CPU
    $ make traces-zip
