open Core
open Blindhen.Sim
open Blindhen.Model
open Blindhen.Types
open Blindhen.PrettyPrinters
open Blindhen.NanoLang

let fork_trace =
  Sequence.of_list
  [
    (* round 1 *)
    Fission (ndiff_xz 1 1, 10); (* 1 -> 1, 2 (1, 0, 1) *)
    (* round 2 *)
    SMove   (lldiff_x 1);       (* 1 move to (1, 0, 0) *)
    Fission (ndiff_z 1, 5);     (* 2 -> 2, 3 (1, 0, 2) *)
    (* round 3 *)
    Fill    (ndiff_x 1);        (* 1 fills (2, 0, 0) *)
    Fill    (ndiff_x 1);        (* 2 fills (2, 0, 1) *)
    Fill    (ndiff_x 1);        (* 3 fills (2, 0, 2) *)
    (* round 4 *)
    FusionS (ndiff_z 1);
    FusionP (ndiff_z (-1));     (* 2 and 1 fuse into 2 (1, 0, 1) *)
    Wait;
    (* round 5 *)
    FusionP (ndiff_z 1);
    FusionS (ndiff_z (-1));     (* 2 and 3 fuse into 2 (1, 0, 1) *)
    (* round 6 *)
    SMove   (lldiff_x (-1));
    (* round 7 *)
    SMove   (lldiff_z (-1));
    (* round 8 *)
    Halt
  ]

let print_trace trace =
  List.map ~f:encode_cmd trace |> List.concat |> String.of_char_list |> print_endline

let () =
  let model = new_matrix 20 in
  let state = initial_state model (new_matrix 20) fork_trace in 
    run_sim state |> ignore
