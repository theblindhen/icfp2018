open Core
open Blindhen.Sim
open Blindhen.Model
open Blindhen.Types
open Blindhen.PrettyPrinters
open Blindhen.NanoLang
open Blindhen.FissionLang

let prg = 
  [Fork(ndiff_x 1, 10, 
     [Move(coord 20 0 0); 
      Barrier; 
      Fork(ndiff_x 1, 5, 
        [Move(coord 30 0 0)])]); 
   Barrier; 
   Fork(ndiff_x 1, 5, 
    [Move(coord 10 0 0)])]

let thread =
  { 
    id = BotId 1;
    pos = coord 0 0 0;
    prg = prg;
    seeds = List.range 2 21 |> List.map ~f:(fun x -> BotId x)
  }

let () =
  let model = new_matrix 40 in
  let trace = flatten_nanotrace (Sequence.of_list (generate_trace thread)) in
  let state = initial_state model (new_matrix 40) trace in 
    run_sim state |> ignore
