open Core
open Blindhen
open Blindhen.Sim
open Blindhen.Types
open Blindhen.NanoLang
open Blindhen.PrettyPrinters

let sim_trace src dst trace =
  let state = initial_state src dst (flatten_nanotrace trace) in 
    run_sim state

let raw_sim raw_file = String.drop_suffix raw_file 8 ^".sim"
let sim_file raw_file = "traces/"^ raw_sim raw_file

let previous_energy raw_file =
  let read_score sim_file =
    if Sys.file_exists sim_file = `Yes then
        let sim_contents = In_channel.create sim_file |> In_channel.input_all in
        match List.hd (String.split sim_contents ~on:'\n') with
        | None -> failwith ("Malformed prev sim file"^ sim_file)
        | Some score_s -> Some (int_of_string score_s)
    else
      None
  in
  let prev_energy = 
    match read_score (sim_file raw_file) with
    | None -> Int.max_value
    | Some en -> en
  in
  let dflt_energy_o = read_score ("dfltTracesF/"^ raw_sim raw_file) in
  (prev_energy, dflt_energy_o)

let write_string_to_file file string =
  let ch = Out_channel.create file in
  Out_channel.output_string ch string;
  Out_channel.close ch

let flip_xz player m =
  let f (x, y, z) = (z, y, x) in
  Model.transform_matrix_coords f m
  |> player
  |> MicroLang.transform_microtrace_coords f

let parallel_player m =
  ParallelPlayer.strategy m
  |> HighLowOptimizer.optimize m

let parallel2d_player nbots_x m =
  ParallelPlayer.strategy_2d m nbots_x
  |> HighLowOptimizer.optimize m

let layer_player subplayer m =
  let layer_strategy =
    match subplayer with
    | `ParallelX  -> ParallelPlayer.strategy_on_region m `X 8
    | `ParallelZ  -> ParallelPlayer.strategy_on_region m `Z 8
    | `Parallel2d -> ParallelPlayer.strategy_2d_on_region m 8 40
  in
  LayerPlayer.strategy m layer_strategy
  |> HighLowOptimizer.optimize m
  

let players =
  [
   ("parallel", parallel_player);
   ("parallel_xz", flip_xz parallel_player);
   ("parallel2d", parallel2d_player 8);
   ("parallel2d_xz", flip_xz (parallel2d_player 8));
   ("layer_parallelx", layer_player `ParallelX);
   ("layer_parallelz", layer_player `ParallelZ);
   ("layer_parallel2d", layer_player `Parallel2d);
   ("parallel2d_40", parallel2d_player 40);
   ("parallel2d_40_xz", flip_xz (parallel2d_player 40))
  ]

type problem =
  | Assemble of Model.matrix
  | Disassemble of Model.matrix
  | Reassemble of Model.matrix * Model.matrix

let load_problem dir raw_file =
  let m = Model.load_model (dir ^ raw_file) in
  match String.prefix raw_file 2 with
  | "FA" -> Assemble m
  | "FD" -> Disassemble m
  | "FR" -> 
      let tgt_file = dir ^ "/" ^ (String.prefix raw_file) 5 ^ "_tgt.mdl" in
      let tgt = Model.load_model tgt_file in Reassemble (m, tgt)
  | _ -> failwith "not supported"

let nanotrace_of_microtrace microtrace =
  Sequence.append
    (MicroLangConverter.to_nanolang_seq microtrace)
    (Sequence.return [(BotId 1, Halt)])

let compete_problem raw_file problem =
  let solutions =
    List.map players ~f:(fun (player_name, player) ->
      let (source, target, microtrace) = match problem with
      | Assemble(target) ->
         (Model.new_matrix (Model.model_size target), Model.copy_matrix target, player target)
      | Disassemble(source) -> 
          (Model.copy_matrix source, Model.new_matrix (Model.model_size source), player source |> MicroLang.invert_microtrace)
      | Reassemble _ -> failwith "Don't call compete with reassemble"
      in
      let nanotrace = nanotrace_of_microtrace microtrace in
      let score = sim_trace source target nanotrace in
      begin match score with
      | None -> Printf.printf "\t%s: player %s failed\n%!" raw_file player_name
      | Some energy -> Printf.printf "\t%s: player %20s used %20s energy\n%!" raw_file player_name (string_of_long_int energy)
      end;
      (if Array.length Sys.argv > 2 && Sys.argv.(2) = "debug" then
        let debug_trace_file = "debug_trace_" ^ player_name ^ ".nbt" in
        write_string_to_file debug_trace_file (String.of_char_list (Sequence.to_list (encode_trace nanotrace)))
      );
      (player_name, microtrace, nanotrace, score))
  in
  List.filter solutions ~f:(fun (_, _, _, score) -> score <> None) 
  |> List.map ~f:(function (name, microtrace, nanotrace, Some score) -> (name, microtrace, nanotrace, score) | _ -> failwith "ERROR")
  |> List.sort ~compare:(fun (_, _, _, s1) (_, _, _, s2) -> Pervasives.compare s1 s2)
  |> List.hd


let trace_model file =
  let dir, raw_file =
    match String.rindex file '/' with
    | None -> ("", file)
    | Some i -> (String.slice file 0 (i+1), String.slice file (i+1) (String.length file)) in
  let Matrix (r, _) = Model.load_model file in
  Printf.printf "----------\n%!";
  Printf.printf "%s: resolution is %d\n%!" raw_file r;
  Out_channel.flush stdout;
  let problem = load_problem dir raw_file in
  let trace_file = "traces/"^ String.drop_suffix raw_file 8 ^".nbt" in
  let prev_energy, dflt_energy_o = previous_energy raw_file in
  let best_solution =
    match problem with
    | Assemble _ | Disassemble _ -> compete_problem raw_file problem
    | Reassemble(source, target) ->
        Printf.printf "%s: Reassembly-problem. Competing on both disassembly and assembly\n" raw_file;
        Printf.printf "\n\t%s: Disassembly\n" raw_file;
        let best_disassemble = compete_problem raw_file (Disassemble(source)) in
        Printf.printf "\t%s: Assembly\n" raw_file;
        let best_assemble    = compete_problem raw_file (Assemble(target)) in
        match best_disassemble, best_assemble with
        | None,_ -> 
           Printf.printf "%s: no player managed to solve the dissassembly!\n%!" raw_file;
           None
        | _,None ->
           Printf.printf "%s: no player managed to solve the assembly!\n%!" raw_file;
           None
        | Some (dis_player, dis_microtrace, _, dis_energy), Some (ass_player, ass_microtrace, _, ass_energy) ->
           let microtrace = Array.append dis_microtrace ass_microtrace in
           Some ("D="^ dis_player ^" and A="^ ass_player, 
                 microtrace,
                 nanotrace_of_microtrace microtrace,
                 dis_energy + ass_energy)
  in
  match best_solution with
  | None ->
     Printf.printf "%s: no player managed to solve the problem!\n%!" raw_file;
     ()
  | Some (player_name, _, trace, new_energy) -> 
      Printf.printf "%s: the best player was %s\n%!" raw_file player_name;
      if new_energy < prev_energy then begin
        Printf.printf "%s: energy usage decreased from %s to %s\n%!" raw_file (string_of_long_int prev_energy) (string_of_long_int new_energy);
        begin
        match dflt_energy_o with
        | None -> ()
        | Some dflt_energy ->
           let est_score = 1. -. (float_of_int new_energy) /. (float_of_int dflt_energy) in
           Printf.printf "\tEstimated score in %% of best: %f\n%!" est_score
        end;
        write_string_to_file trace_file (String.of_char_list (Sequence.to_list (encode_trace trace)));
        write_string_to_file (sim_file raw_file) (string_of_int new_energy)
      end
      else
        if new_energy = prev_energy then
          Printf.printf "%s: energy usage is the same at %s\n" raw_file (string_of_long_int prev_energy)
        else
          Printf.printf "%s: energy usage increased from %s to %s\n" raw_file (string_of_long_int prev_energy) (string_of_long_int new_energy)

let () =
  if Array.length Sys.argv < 2 then failwith "Usage: a model file or a directory";
  let arg = Sys.argv.(1) in
  if Sys.is_directory arg = `No then
    trace_model arg
  else
    let files = Sys.readdir arg |> Array.to_list |> List.sort ~compare:(String.compare) in
    (* trace through FA files *)
    List.iter files ~f:(fun file ->
        if (not (String.is_prefix ~prefix:"FR" file) || String.is_suffix ~suffix:"_src.mdl" file) 
              && String.is_suffix ~suffix:".mdl" file then
          trace_model (arg ^"/"^ file))
    (* TODO: Trace through FR in this app? *)
