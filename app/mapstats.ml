open Core
open Blindhen
open Blindhen.Sim
open Blindhen.Types
open Blindhen.Model
open Blindhen.NanoLang

let raw_file file =
  match String.rindex file '/' with
  | None -> ("", file)
  | Some i -> (String.slice file 0 i, String.slice file (i+1) (String.length file))

let count_bits_set b = 
  Bitv.fold_left (fun acc bit -> if bit then acc+1 else acc) 0 b

let percentage x y = (float_of_int x) /. (float_of_int y) *. 100.0

let model_stats file =
  let dir, filename = raw_file file in
  if String.is_prefix ~prefix:"FR" filename then begin
    if String.is_suffix ~suffix:"_tgt.mdl" filename then () else begin
      let tgt_file = dir ^ "/" ^ (String.prefix filename) 5 ^ "_tgt.mdl" in
      let Matrix (_, src_mat) = Model.load_model file in
      let Matrix (r, tgt_mat) = Model.load_model tgt_file in
      let changed_voxels = Bitv.bw_xor src_mat tgt_mat |> count_bits_set in
      let filled_voxels = src_mat |> count_bits_set in
      let total_voxels = r * r * r in
      Printf.printf "%s: r=%d, changed %d out of %d total voxels (%.1f%%) and %d intially filled voxels (%.1f%%)\n" 
        filename r changed_voxels total_voxels (percentage changed_voxels total_voxels) filled_voxels (percentage changed_voxels filled_voxels);
      if (same_bitv (Bitv.bw_or src_mat tgt_mat) src_mat) then
        Printf.printf "%s: only removed voxels\n" filename;
      if (same_bitv (Bitv.bw_or src_mat tgt_mat) tgt_mat) then
        Printf.printf "%s: only added voxels\n" filename;
      Out_channel.flush stdout
    end
  end else begin
    let Matrix (r, _) = Model.load_model file in
    Printf.printf "%s: %d\n" filename r;
    Out_channel.flush stdout
  end

let () =
  if Array.length Sys.argv < 2 then failwith "Usage: a model file or a directory";
  let arg = Sys.argv.(1) in
  if Sys.is_directory arg = `No then
    model_stats arg
  else
    let files = Sys.readdir arg |> Array.to_list |> List.sort ~compare:(String.compare) in
    List.iter files ~f:(fun file ->
        if String.is_suffix ~suffix:".mdl" file then
          model_stats (arg ^"/"^ file))
