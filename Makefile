APP_ML_FILES = $(wildcard app/*.ml)
APP_EXE_FILES = $(APP_ML_FILES:.ml=.exe)

TRACER_EXE = _build/default/app/tracer.exe

all:
	dune build $(APP_EXE_FILES)

clean:
	dune clean

test:
	dune runtest

TRACER_EXE: all

ALL_TRACES = $(shell cd problemsF && ls *.mdl | sort -r | sed 's/_...[.]mdl/.nbt/' | sed 's%F%traces/F%')

.PHONY: all_traces
all_traces: $(ALL_TRACES)

SMALL_TRACES = $(shell cd problemsF && ls F[ADR]0[01234]*.mdl | sort -r | sed 's/_...[.]mdl/.nbt/' | sed 's%F%traces/F%')

.PHONY: small_traces
small_traces: $(SMALL_TRACES)

traces/FA%.nbt: problemsF/FA%_tgt.mdl $(TRACER_EXE)
	$(TRACER_EXE) $<
	touch $@

traces/FD%.nbt: problemsF/FD%_src.mdl $(TRACER_EXE)
	$(TRACER_EXE) $<
	touch $@

traces/FR%.nbt: problemsF/FR%_src.mdl $(TRACER_EXE)
	$(TRACER_EXE) $<
	touch $@

source-zip:
	rm -rf source-tmp source-tmp.zip
	mkdir -p source-tmp/theblindhen submissions
	git archive HEAD | tar -C source-tmp/theblindhen -xp \
	  --exclude "problemsL*" \
	  --exclude "problemsF*" \
	  --exclude "dfltTracesL*" \
	  --exclude "submissions*"
	touch -d "2018-07-20 18:00:00" `find source-tmp`
	cd source-tmp && zip -X -r --latest-time ../source-tmp.zip theblindhen
	touch source-tmp.zip
	shasum -a 256 source-tmp.zip
	mv source-tmp.zip \
	   submissions/source-`shasum -a 256 source-tmp.zip \
	                       | grep -o '^.......'`.zip
	rm -rf source-tmp

sim-traces-zip:
	rm -rf traces-tmp traces-tmp.zip
	mkdir -p traces-tmp submissions
	cp -a traces/F*.nbt traces/F*.sim traces-tmp
	touch -d "2018-07-20 18:00:00" `find traces-tmp`
	cd traces-tmp && zip -X -r --latest-time ../traces-tmp.zip *
	touch traces-tmp.zip
	shasum -a 256 traces-tmp.zip
	mv traces-tmp.zip \
	   submissions/sim-traces-`shasum -a 256 traces-tmp.zip \
	                           | grep -o '^.......'`.zip
	rm -rf traces-tmp

traces-zip:
	rm -rf traces-tmp traces-tmp.zip
	mkdir -p traces-tmp submissions
	cp -a traces/F*.nbt traces-tmp
	touch -d "2018-07-20 18:00:00" `find traces-tmp`
	cd traces-tmp && zip -X -r --latest-time ../traces-tmp.zip *
	touch traces-tmp.zip
	shasum -a 256 traces-tmp.zip
	mv traces-tmp.zip \
	   submissions/traces-`shasum -a 256 traces-tmp.zip \
	                       | grep -o '^.......'`.zip
	rm -rf traces-tmp
