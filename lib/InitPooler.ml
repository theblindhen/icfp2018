open Core
open Types
open MicroLang
open MicroLangHelper
open TracerHelpers
open PrettyPrinters
open Model

(* Takes the number of bots and how many seeds they each should have afterwards. *)
(* In an element of `bots`, the first int is an absolute position on `axis`, and
 * the second is the number of seeds that should be left in the bot.
 * If there are more remaining seeds than specified, the leftover seeds will
 * remain in the first bot. *)
let pool_me axis init_pos (bots : (int * int) list) =
  let ldiff_t =
    match axis with
    | `X -> ldiff_x
    | `Y -> ldiff_y
    | `Z -> ldiff_z in
  let axis_of_coord (Coord (x,y,z)) =
    match axis with
    | `X -> x
    | `Y -> y
    | `Z -> z in
  let coord_t dt = init_pos +: (ldiff_t dt) in
  let move_bot_right cur_t to_t =
    let tr, _ = trace_linear_move (ldiff_t (to_t - cur_t)) (coord_t cur_t) in
    microtrace_of_singular_trace tr
  in
  let rec fission_to_right (cur_t : int) (children : (int * int) list) =
    match children with
    | [] -> [||]
    | _ ->
      let nchildren = List.length children in
      let nleft = (nchildren - 1)/2 in
      let nright = nchildren - 1 - nleft in
      let ch_left, (son_t, son_seeds), ch_right =
        let ch_left, rem = List.split_n children nleft in
        match rem with
        | [] -> failwith "This should never happen"
        | hd::tl -> ch_left, hd, tl
      in
      let seeds_right = son_seeds + nright + (ch_right |> List.map ~f:snd |> List.fold ~init:0 ~f:(+)) in
      let the_fission = [| [Fission (coord_t cur_t, ndiff_of_diff (ldiff_t 1), seeds_right)] |] in
      let move_my_son =
        move_bot_right (cur_t+1) son_t
        |> (fun tr -> add_waiting_bots tr [coord_t cur_t])
      in
      let sub_fissions =
        match (fission_to_right cur_t ch_left, fission_to_right son_t ch_right) with
        | [||], [||] -> [||]
        | [||], f_right -> add_waiting_bots f_right [coord_t cur_t]
        | f_left, [||]  -> add_waiting_bots f_left  [coord_t son_t]
        | f_left, f_right -> weave_two_traces_exn f_left f_right
      in
      Array.concat [ the_fission ; move_my_son ; sub_fissions ]
  in
  (* Printf.printf "Pooling from %s to bots at %s\n" (string_of_coord init_pos) (bots |> List.map ~f:fst |> List.map ~f:string_of_int |> string_of_list); *)
  let bots =
    (* Make the axis-positions into offsets *)
    List.map bots ~f:(fun (u, seeds) -> (u - axis_of_coord init_pos, seeds))
  in
  (* Printf.printf "Pooling from %s to bots at %s\n" (string_of_coord init_pos) (bots |> List.map ~f:fst |> List.map ~f:string_of_int |> string_of_list); *)
  match bots with
  | [] -> failwith "Gotta have a bot!"
  | (first_bot,_)::bots ->
     let first_moves = move_bot_right 0 first_bot in
     let trace = fission_to_right first_bot bots in
     Array.append first_moves trace


let%test "pool_test" =
  let microtrace = pool_me `X (coord 0 0 0) [ (0,0) ; (1, 0); (2, 0) ] in
  microtrace = [| [ Fission(coord 0 0 0, ndiff_x 1, 1)];
                  [ Fission(coord 1 0 0, ndiff_x 1, 0); Wait(coord 0 0 0) ] |]
  (* Printf.printf "\n\n%s\n" (string_of_microtrace microtrace); *)
  (* false *)

let%test "pool_test2" =
  let target_xs = List.init 40 ~f:(fun i -> (30 + 3*i, 1)) in
  let microtrace = pool_me `X (coord 0 0 0) target_xs in
  let bots = bots_after_trace microtrace in
  let bot_pos = List.sort ~compare:(fun (Coord (x1,_,_)) (Coord (x2,_,_)) -> Int.compare x1 x2) bots in
  List.length bots = 40 && bot_pos = List.map target_xs ~f:(fun (x,_) -> coord x 0 0)
  (* Printf.printf "\n\n%s\n" (string_of_microtrace microtrace);
   * false *)

let%test "pool_test3" =
  let microtrace = pool_me `Z (coord 1 2 3) [ (3,1) ; (4, 3); (5, 5) ] in
  microtrace = [| [ Fission(coord 1 2 3, ndiff_z 1, 9)];
                  [ Fission(coord 1 2 4, ndiff_z 1, 5); Wait(coord 1 2 3) ] |]
  (* Printf.printf "\n\n%s\n" (string_of_microtrace microtrace);
   * false *)


(*
let%test "pool_test3" =
  let microtrace = pool_me [0; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11; 12; 13; 14; 15; 16; 17; 18; 19; 20; 21; 22; 23; 24; 25; 26; 27; 28] in
  (* microtrace = [| [ Fission(coord 0 0 0, ndiff_x 1, 1)];
   *                 [ Fission(coord 1 0 0, ndiff_x 1, 0); Wait(coord 0 0 0) ] |] *)
  Printf.printf "\n\n%s\n" (string_of_microtrace microtrace);
  false
*)

