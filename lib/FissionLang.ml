open Core
open Types
open NanoLang
open TracerHelpers
open SimHelpers

type fission_stm =
    (* we assume all Move coords are of the form (x, 0, 0) *)
  | Move of coord
  | Fork of ndiff * int * (fission_stm list)
  | Barrier

type fission_prg = fission_stm list

type thread = {
  id: botid;
  mutable pos: coord;
  mutable prg: fission_prg;
  mutable seeds: botid list; 
}

type fission_state = {
  mutable threads: thread list;
}

let is_done (s : fission_state) =
  List.for_all ~f:(fun { prg = prg; _ } -> prg = []) (s.threads) 

let all_at_barrier (s : fission_state) =
  List.for_all ~f:(function { prg = Barrier::_;_  } | { prg = []; _ } -> true | _ -> false) (s.threads) 

let drop_head_of_thread (t : thread) = match t.prg with
  | [] -> ()
  | _::tl -> t.prg <- tl

let step_thread (s : fission_state) (t : thread) = match t.prg with
  | [] | Barrier::_ -> Wait
  | Fork(nd, m, prg)::tl ->
      let new_id, primary_seeds, secondary_seeds = partion_seeds t.seeds m in
      let new_thread = { id = new_id; pos = t.pos +: nd; prg = prg; seeds = secondary_seeds } in
      s.threads <- new_thread :: s.threads;
      t.seeds <- primary_seeds;
      t.prg <- tl;
      Fission(nd, m)
  | Move(dst)::tl ->
      let diff = dst -: t.pos in
      if mlen(diff) <= 15 then begin
        t.prg <- tl;
        t.pos <- dst;
        if mlen(diff) = 0 then Wait else SMove(lldiff_of_diff diff)
      end else begin
        let ld = 15 *: direction_of_diff diff |> lldiff_of_diff in
        t.pos <- t.pos +: ld;
        SMove(ld)
      end

let step_machine (s : fission_state) : (botid * command) list =
  if all_at_barrier s then begin
    List.iter ~f:drop_head_of_thread s.threads; []
  end else
    List.map ~f:(fun t -> (t.id, step_thread s t)) s.threads

let rec generate_trace (s : fission_state) : nanotrace =
  if is_done s then [] else
    match step_machine s with
    | [] -> generate_trace s
    | cmds -> cmds :: generate_trace s

let generate_trace (t : thread) : nanotrace =
  generate_trace { threads = [t] }
