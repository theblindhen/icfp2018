open Core
open Types

type botid = BotId of int
  [@@deriving compare]

type command = 
    | Halt
    | Wait
    | Flip
    | SMove of lldiff
    | LMove of sldiff * sldiff
    | Fission of ndiff * int
    | Fill of ndiff
    | Void of ndiff
    | FusionP of ndiff
    | FusionS of ndiff
    | GFill of ndiff * fdiff
    | GVoid of ndiff * fdiff

type nanotrace = ((botid * command) list) list
type nanoseq = ((botid * command) list) Sequence.t

let encode_sldiff (Diff (dx, dy, dz) : sldiff) =
    if dx <> 0 then (0b01, dx+5) else
    if dy <> 0 then (0b10, dy+5) else
    if dz <> 0 then (0b11, dz+5) else
    failwith "Invalid short linear difference"

let encode_lldiff (Diff (dx, dy, dz) : lldiff) =
    if dx <> 0 then (0b01, dx + 15) else
    if dy <> 0 then (0b10, dy + 15) else
    if dz <> 0 then (0b11, dz + 15) else
    failwith "Invalid long linear difference"

let encode_ndiff (Diff (dx, dy, dz) : ndiff) =
    (dx + 1) * 9 + (dy + 1) * 3 + (dz + 1)

let encode_fdiff (Diff (dx, dy, dz) : fdiff) =
  [
    char_of_int (dx + 30);
    char_of_int (dy + 30);
    char_of_int (dz + 30);
  ]

let encode_cmd cmd = match cmd with
    | Halt -> [char_of_int 0b1111_1111]
    | Wait -> [char_of_int 0b1111_1110]
    | Flip -> [char_of_int 0b1111_1101]
    | SMove lld -> 
        let (a, i) = encode_lldiff lld in 
            [char_of_int ((a lsl 4) + 0b0100); char_of_int i]
    | LMove (sld1, sld2) -> 
        let (a1, i1) = encode_sldiff sld1 in
        let (a2, i2) = encode_sldiff sld2 in
            [char_of_int ((a2 lsl 6) + (a1 lsl 4) + 0b1100); 
            char_of_int ((i2 lsl 4) + i1)]
    | FusionP nd -> [char_of_int (((encode_ndiff nd) lsl 3) + 0b111)]
    | FusionS nd -> [char_of_int (((encode_ndiff nd) lsl 3) + 0b110)]
    | Fission (nd, m) -> [char_of_int (((encode_ndiff nd) lsl 3) + 0b101); char_of_int m]
    | Fill nd -> [char_of_int (((encode_ndiff nd) lsl 3) + 0b011)]
    | Void nd -> [char_of_int (((encode_ndiff nd) lsl 3) + 0b010)]
    | GFill (nd, fd) ->
        char_of_int (((encode_ndiff nd) lsl 3) + 0b001) ::
        encode_fdiff fd
    | GVoid (nd, fd) ->
        char_of_int (((encode_ndiff nd) lsl 3) + 0b000) ::
        encode_fdiff fd

let sort_cmds = List.sort ~compare:(fun (BotId b1, _) (BotId b2, _) -> Int.compare b1 b2)

let encode_cmds cmds = 
    let sorted_cmds = sort_cmds cmds in
    let encoded_cmds = List.map ~f:(fun (_, cmd) -> encode_cmd cmd) sorted_cmds in
        List.concat encoded_cmds

let encode_trace (trace : nanoseq) =
  Sequence.map ~f:encode_cmds trace
  |> Sequence.concat_map ~f:Sequence.of_list

let flatten_nanotrace (trace : nanoseq) =
  Sequence.concat_map trace ~f:(fun cmds ->
    sort_cmds cmds
    |> Sequence.of_list
    |> Sequence.map ~f:snd
  )

let invert_cmd (botid, cmd) = match cmd with
| Halt -> failwith "halts should be handled separately"
| Wait -> [(botid, Wait)]
| Flip -> [(botid, Flip)]
| SMove(lld) -> [(botid, SMove(invert_diff lld))]
| LMove(sld1, sld2) -> [(botid, LMove(invert_diff sld2, invert_diff sld1))]
| Fill(nd) -> [(botid, Void(nd))]
| Void(nd) -> [(botid, Fill(nd))]
| Fission _ | FusionP _ | FusionS _ | GFill _ | GVoid _ ->
    failwith "not implemented yet"

let invert_cmds (cmds : (botid * command) list) = 
  List.map ~f:invert_cmd cmds |> List.concat

let remove_halt (trace : nanotrace) =
  match List.rev trace with
  | [(_, Halt)]::tl -> List.rev tl
  | _ -> failwith "expected trace to start & end with a single bot and end with a halt"

let invert_trace (trace : nanotrace) : nanotrace =
  let rev_trace = List.rev trace in
  match trace, rev_trace with
  | [(id, _)]::_, [(_, Halt)]::tl -> List.map ~f:invert_cmds tl @ [[(id, Halt)]]
  | _ -> failwith "expected trace to start & end with a single bot and end with a halt"

let%test "encode <12,0,0>" =
    encode_lldiff (lldiff_of_triple (12, 0, 0)) = (0b01, 0b11011)

let%test "encode SMove <12,0,0>" =
    encode_cmd (SMove (lldiff_of_triple (12, 0, 0))) = [char_of_int 0b00010100; char_of_int 0b00011011]

let%test "encode SMove <0,0,-4>" =
    encode_cmd (SMove (lldiff_of_triple (0, 0, -4))) = [char_of_int 0b00110100; char_of_int 0b00001011]

let%test "encode LMove <3,0,0> <0,-5,0>" =
    encode_cmd (LMove(sldiff_of_triple (3, 0, 0), sldiff_of_triple (0, -5, 0))) = [char_of_int 0b10011100; char_of_int 0b00001000]

let%test "encode LMove <0,-2,0> <0,0,2>" =
    encode_cmd (LMove(sldiff_of_triple (0, -2, 0), sldiff_of_triple (0, 0, 2))) = [char_of_int 0b11101100; char_of_int 0b01110011]

let%test "encode FusionP <-1,1,0>" =
    encode_cmd (FusionP (ndiff_of_triple (-1, 1, 0))) = [char_of_int 0b00111111]

let%test "encode FusionS <1,-1,0>" =
    encode_cmd (FusionS (ndiff_of_triple (1, -1, 0))) = [char_of_int 0b10011110]

let%test "encode Fission <0,0,1> 5" =
    encode_cmd (Fission (ndiff_of_triple (0, 0, 1), 5)) = [char_of_int 0b01110101; char_of_int 0b00000101]

let%test "encode Fill <0,-1,0>" =
    encode_cmd (Fill (ndiff_of_triple (0, -1, 0))) = [char_of_int 0b01010011]

let%test "encode GFill <0,-1,0> <10,-15,20>" =
    encode_cmd (
      GFill (ndiff_of_triple (0, -1, 0), fdiff_of_triple (10, -15, 20))
    ) = [
      char_of_int 0b01010001; char_of_int 0b00101000;
      char_of_int 0b00001111; char_of_int 0b00110010;
    ]

let%test "encode GVoid <1,0,0> <5,5,-5>" =
    encode_cmd (
      GVoid (ndiff_of_triple (1, 0, 0), fdiff_of_triple (5, 5, -5))
    ) = [
      char_of_int 0b10110000; char_of_int 0b00100011;
      char_of_int 0b00100011; char_of_int 0b00011001;
    ]

let%test "encode_trace [(2, Fill <0,-1,0>), (1, FusionS <1,-1,0>)]" =
  let cmd2 = Fill (ndiff_of_triple (0, -1, 0)) in
  let cmd1 = Fission (ndiff_of_triple (0, 0, 1), 5) in
    Sequence.to_list (encode_trace (Sequence.of_list [[(BotId 2, cmd2); (BotId 1, cmd1)]])) =
      encode_cmd cmd1 @ encode_cmd cmd2
