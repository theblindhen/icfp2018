(*
    Helpers and combinators for microtraces.
*)

open Core
open Types
open MicroLang
open TracerHelpers
open PrettyPrinters


(* Given a region, apply the strategy_on_region to the sub-region which actually
   contains any filled voxels. *)
let strategy_on_optimized_region (m : Model.matrix) (strategy_on_region : region -> microtrace) (Region (r1, r2) as region) =
  let Region (or1, or2) as opt_region = Model.optimize_region m region in
  (* Printf.printf "Strategy on region %s optimized to %s\n" (string_of_region region) (string_of_region opt_region); *)
  let tr_to_opt =
    ([], r1)
    >>: trace_diff_move_xzy (or1 -: r1)
    |> fst |> microtrace_of_singular_trace in
  let opt_out_to_r_out =
    let out_pos (Region (Coord (rx1,_,rz1), Coord (_,ry2,_))) = coord rx1 (ry2+1) rz1 in
    ([], out_pos opt_region)
    >>: trace_diff_move_zxy ((out_pos region) -: (out_pos opt_region))
    |> fst |> microtrace_of_singular_trace
  in
  Array.concat [ tr_to_opt ; strategy_on_region opt_region ; opt_out_to_r_out ]
