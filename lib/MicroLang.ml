open Core
open Types

type microcmd = 
    | Wait of coord
    | Flip of coord 
    | SMove of coord * lldiff
    | LMove of coord * sldiff * sldiff
    | Fission of coord * ndiff * int
    | Fuse of coord * ndiff * int      (* int indicates number of seeds provided by the secondary fuser *)
    | Fill of coord * ndiff
    | Void of coord * ndiff

type microtrace = (microcmd list) array

let invert_micro_cmd (c : microcmd) : microcmd = match c with
| Wait(c) -> Wait(c)
| Flip(c) -> Flip(c)
| SMove(c, lld) -> SMove(c +: lld, invert_diff lld)
| LMove(c, sld1, sld2) -> LMove(c +: sld1 +: sld2, invert_diff sld2, invert_diff sld1)
| Fission(c, nd, m) -> Fuse(c, nd, m)
| Fuse(c, nd, m) -> Fission(c, nd, m)
| Fill(c, nd) -> Void(c, nd)
| Void(c, nd) -> Fill(c, nd)

let invert_microtrace (t : microtrace) : microtrace =
  let inverted_cmds = Array.map ~f:(List.map ~f:invert_micro_cmd) t in
  Array.rev_inplace inverted_cmds;
  inverted_cmds

let transform_cmd_coords f (c : microcmd) : microcmd = match c with
| Wait(c) -> Wait(transform_coord f c)
| Flip(c) -> Flip(transform_coord f c)
| SMove(c, lld) -> SMove(transform_coord f c, transform_diff f lld)
| LMove(c, sld1, sld2) -> LMove(transform_coord f c, transform_diff f sld1, transform_diff f sld2)
| Fission(c, nd, m) -> Fission(transform_coord f c, transform_diff f nd, m)
| Fuse(c, nd, m) -> Fuse(transform_coord f c, transform_diff f nd, m)
| Fill(c, nd) -> Fill(transform_coord f c, transform_diff f nd)
| Void(c, nd) -> Void(transform_coord f c, transform_diff f nd)

let transform_microtrace_coords f (t : microtrace) : microtrace =
  Array.map ~f:(List.map ~f:(transform_cmd_coords f)) t