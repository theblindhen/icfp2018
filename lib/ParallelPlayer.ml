open Core
open Types
open MicroLang
open MicroLangHelper
open TracerHelpers
open PrettyPrinters
open StrategyHelpers

let corners_to_ranges bot_ts end_t =
  List.fold_left ~init:(List.hd_exn bot_ts, []) (List.tl_exn bot_ts @ [end_t+1])
    ~f:(fun (prev_t, tranges) this_t -> (this_t, (prev_t, this_t-1)::tranges) )
  |> snd
  |> List.rev

let strategy_on_region (m : Model.matrix) axis nbots (Region (main_r1,main_r2) as main_region) =
  (* Printf.printf "Strat on region %s with %d bots\n" (string_of_region main_region) nbots; *)
  assert (nbots > 0);
  (* Assumes a bot begins at main_r1.
     Leaves the bot at (rx1, ry2+1, rz1), i.e. directly above where it started *)
  let t_of_coord (Coord (x,_,z)) = 
    match axis with
    | `X -> x
    | `Z -> z in
  let trace_region region =
    CleverFill.trace_region_above_lazy m region
    |> fst |> microtrace_of_singular_trace
  in
  let bot_ts = DivideMap.divide_along_axis ~suppress_warning:(Some ()) m main_region axis nbots in 
  (* Printf.printf "Box (%d) %s\n" (List.length bot_ts) (bot_ts |> List.map ~f:string_of_int |> string_of_list); *)
  let pool_trace = InitPooler.pool_me axis main_r1 (List.map bot_ts ~f:(fun t -> (t,0))) in
  let tregions = corners_to_ranges bot_ts (t_of_coord main_r2) in
  let region_trace =
    List.map tregions ~f:(fun (t1,t2) ->
        slice_region axis main_region t1 t2
        |> strategy_on_optimized_region m trace_region
      )
    |> weave_traces_exn
  in
  let pool_trace_inv =
    (* We're inverting the pooling at ry2+1 *)
    let Coord (_,main_ry1,_), Coord (_,main_ry2,_) = main_r1, main_r2 in
    pool_trace
    |> MicroLang.invert_microtrace
    |> MicroLangHelper.shift_microtrace (ldiff_y (main_ry2 + 1 - main_ry1))
  in
  Array.concat [ pool_trace ; region_trace ; pool_trace_inv ]
  (* Array.concat [ pool_trace ; MicroLang.invert_microtrace pool_trace ] *)



let microtrace_down r =
  let trace_down,_ = trace_linear_move (ldiff_y (-(r-1))) (coord 0 (r-1) 0) in
  microtrace_of_singular_trace trace_down

let strategy (Matrix (r,_) as m : Model.matrix) =
  let trace_region = strategy_on_region m `X 40 (Model.entire_active_region m) in
  Array.append trace_region (microtrace_down r)

let strategy_z (Matrix (r,_) as m : Model.matrix) =
  let trace_region = strategy_on_region m `Z 40 (Model.entire_active_region m) in
  Array.append trace_region (microtrace_down r)

let strategy_2d_on_region (m : Model.matrix)  nbots_on_x total_bots (Region (r1, r2) as region) =
  (* Assumes the bot is in the lln corner of region.
     Ends with the bot in the same (x,z) but at y = r2.y+1.
   *)
  let bots_x = DivideMap.divide_along_axis ~suppress_warning:(Some ()) m region `X nbots_on_x in
  let nbots_x = List.length bots_x in
  let bot_seeds =
    let seeds_per_bot = total_bots/nbots_x - 1 in
    let bigs = total_bots - nbots_x - seeds_per_bot * nbots_x in
    (List.init bigs ~f:(fun _ -> seeds_per_bot + 1)) @ (List.init (nbots_x - bigs) ~f:(fun _ -> seeds_per_bot))
  in
  (* Printf.printf "bots_seeds: %s\n" (bot_seeds |> List.map ~f:string_of_int |> string_of_list);
   * Printf.printf "Used bots: %d\n" (nbots_x + (bot_seeds |> List.fold ~init:0 ~f:(+))); *)
  assert (nbots_x + (bot_seeds |> List.fold ~init:0 ~f:(+)) = total_bots);
  let bots_seeds_x = List.zip_exn bots_x bot_seeds in
  let pool_trace = InitPooler.pool_me `X r1 bots_seeds_x in
  (* Printf.printf "Bot xs: %s" (bots_x |> List.map ~f:string_of_int |> string_of_list);
   * Printf.printf "Pool str: %s" (string_of_microtrace pool_trace); *)
  let slice_traces =
    let Coord (rx2,_,_) = r2 in
    List.zip_exn (corners_to_ranges bots_x rx2) bot_seeds
    |> List.map ~f:(fun ((x1,x2), seeds) ->
           let slice = slice_region `X region x1 x2 in
           (* Printf.printf "Strategy2d on x-range %d-%d (%s) \n" x1 x2 (string_of_region slice); *)
           strategy_on_region m `Z (seeds+1) slice
        )
    |> weave_traces_exn
  in
  let pool_trace_inv =
    (* We're inverting the pooling at upper y+1 *)
    let Coord (_,ry1,_), Coord (_,ry2,_) = r1, r2 in
    pool_trace
    |> MicroLang.invert_microtrace
    |> MicroLangHelper.shift_microtrace (ldiff_y (ry2-ry1+1))
  in
  Array.concat [ pool_trace ; slice_traces ; pool_trace_inv ]

let strategy_2d (Matrix (r,_) as m : Model.matrix) nbots_on_x =
  let trace = strategy_on_optimized_region m (strategy_2d_on_region m nbots_on_x 40) (Model.entire_active_region m) in
  Array.append trace (microtrace_down r)
