open Core
open Types
open Model
open NanoLang
open SimTypes
open PrettyPrinters

let validate_coord_is_void (s : state) (c : coord) = 
  if is_filled s.matrix c then begin
    raise (BadTrace (
      sprintf "Coordinate %s not void" (string_of_coord c)
    ))
  end

let validate_coord (s : state) (Coord (x, y, z) as c) =
  let Matrix (r, _) = s.goal in
  if not (0 <= x && x < r && 0 <= y && y < r && 0 <= z && z < r) then begin
    raise (BadTrace (
      sprintf "Coordinate %s out of bounds" (string_of_coord c)
    ))
  end

let validate_region_is_void (s : state) (r : region) =
  coords_of_region r
  |> OSeq.iter (validate_coord_is_void s)

let region_operation = function
  | GFill _ -> `Fill
  | GVoid _ -> `Void
  | _ -> failwith "Not a GFill or GVoid"

let singleton_cmd (cmd : command) = match cmd with
  | Halt | Wait | Flip | SMove _ | LMove _ | Fission _ | Fill _
  | Void _ -> true
  | FusionS _ | FusionP _ | GFill _ | GVoid _ -> false

let group_cmd_target_coord (cmd : command) (bot : nanobot) = match cmd with
| FusionP nd -> bot.coord +: nd
| FusionS nd -> bot.coord +: nd
| (GFill (nd, fd) | GVoid (nd, fd)) ->
    begin match region (bot.coord +: nd) (bot.coord +: nd +: fd) with
    | Region (first, _) -> first
    end
| _ -> failwith "Singleton command"

let match_group_cmds (l : (command * nanobot) list) : cmd_group list =
  let target_coords = Coord.Table.create () in
  (* Build a hash table of the target coordinates of each grouped command.
   * For GFill/GVoid, the target is the first Region corner. *)
  List.iter l ~f:(fun (cmd, bot) ->
    let target = group_cmd_target_coord cmd bot in
    Hashtbl.add_multi target_coords ~key:target ~data:(cmd, bot)
  );
  (* match up each primary grouped command with its seconary counterpart *)
  List.map l ~f:(fun (cmd, bot) -> match cmd with
    | FusionP ndp -> 
        begin match Hashtbl.find_multi target_coords bot.coord with
        | [FusionS nds, bots] ->
            if bots.coord +: nds = bot.coord then
              [FusionPair { primary = (ndp, bot); secondary = (nds, bots) }]
            else raise (BadTrace "No matching FusionS")
        | _::_::_ -> raise (BadTrace "Multiple matching FusionS")
        | _ -> raise (BadTrace "No matching FusionS")
        end
    | FusionS _ -> []
    | (GFill (nd, fd) | GVoid (nd, fd)) ->
        let operation = region_operation cmd in
        let r = region (bot.coord +: nd) (bot.coord +: nd +: fd) in
        let Region (key_corner, _) = r in
        let all_bots = Hashtbl.find_multi target_coords key_corner in
        if bot.coord +: nd <> key_corner then begin
          (* This is not the lower-left-corner bot. Do a sanity check to make
           * sure such a bot exists. *)
          let in_key_corner = function
            | (GFill (nd', _), bot' |
               GVoid (nd', _), bot') ->
                 bot'.coord +: nd' = key_corner
            | _ -> false
          in
          if not (List.exists all_bots ~f:in_key_corner) then
            raise (BadTrace "No bot in key corner");
          []
        end else begin
          if List.length all_bots <> Int.pow 2 (region_dim r) then
            raise (BadTrace "Too few bots for dimension");
          let targeted_corners = Coord.Hash_set.create () in
          List.iter all_bots ~f:(fun (cmd', bot') ->
            match cmd' with
            | (GFill (nd', fd') | GVoid (nd', fd')) ->
                if not (r = region (bot'.coord +: nd')
                                   (bot'.coord +: nd' +: fd'))
                then
                  raise (BadTrace "Group disagreement on region");
                if region_operation cmd' <> operation then
                  raise (BadTrace "Group disagreement on operation");
                Hash_set.add targeted_corners (bot'.coord +: nd')
            | _ -> raise (BadTrace "Different group commands interfere")
          );
          if Hash_set.length targeted_corners <> List.length all_bots then
            raise (BadTrace "Not all different corners");
          [ GroupRegion {
              region = r;
              operation;
              bots = List.map ~f:snd all_bots
          } ]
        end
    | _ -> failwith "Singleton command"
    )
  |> List.concat

let group_cmds (l : (command * nanobot) list) : cmd_group list = 
  let (singleton_cmds, group_cmds) =
    List.partition_tf ~f:(Fn.compose singleton_cmd fst) l in
  (List.map ~f:(fun (cmd, bot) -> Singleton(cmd, bot)) singleton_cmds) @
  (match_group_cmds group_cmds)

(* internal functions *)
(** Calls `f` with every coordinate between `coord` and `coord + diff`,
 * end point NOT INCLUSIVE. *)
let iter_diff ~f coord (diff: [> `ll ] gen_diff) =
  let direction = direction_of_diff diff in
  let end_coord = coord +: diff in
  let cur_coord = ref coord in
  while Coord.compare !cur_coord end_coord <> 0 do
    f !cur_coord;
    cur_coord := !cur_coord +: direction
  done

let iter_volatile_coords
    ~f:(f: coord -> unit) cmd_group : unit =
  match cmd_group with
  | Singleton (Halt, bot) -> f bot.coord
  | Singleton (Wait, bot) -> f bot.coord
  | Singleton (Flip, bot) -> f bot.coord
  | Singleton (SMove lldiff, bot) ->
      iter_diff ~f bot.coord lldiff;
      f (bot.coord +: lldiff)
  | Singleton (LMove (sld1, sld2), bot) ->
      iter_diff ~f bot.coord sld1;
      iter_diff ~f (bot.coord +: sld1) sld2;
      f (bot.coord +: sld1 +: sld2)
  | Singleton (Fission (nd, _), bot) ->
      f bot.coord;
      f (bot.coord +: nd)
  | Singleton (Fill nd, bot) ->
      f bot.coord;
      f (bot.coord +: nd)
  | Singleton (Void nd, bot) ->
      f bot.coord;
      f (bot.coord +: nd)
  | Singleton ((FusionP _ | FusionS _ | GFill _ | GVoid _), _) ->
      failwith ("Fusion command made it into interference check "^
                "without becoming a FusionPair")
  | FusionPair { primary = (_, botp); secondary = (_, bots) } ->
      f botp.coord;
      f bots.coord
  | GroupRegion { region; bots; operation=_ } ->
      List.iter ~f:(fun bot -> f bot.coord) bots;
      OSeq.iter f (coords_of_region region)

let validate_interference (cmds : cmd_group list) : unit =
  let used_coords = Coord.Hash_set.create () in
  List.iter cmds ~f:(fun group ->
    iter_volatile_coords group ~f:(fun coord ->
      match Hash_set.strict_add used_coords coord with
      | Ok () -> ()
      | Error _ -> raise (BadTrace "Interference!")
    )
  )

let step_singleton_cmd (s : state) (c : command) (bot : nanobot) : unit = match c with
  | Halt ->
      if List.length s.bots <> 1 then 
        raise (BadTrace "There should be exactly one bot") else
      if bot.coord <> origin then
        raise (BadTrace "Bot is not at origin") else
      if s.harmonics <> Low then
        raise (BadTrace "halt: Harmonics should be low") else
      s.bots <- []
  | Wait -> ()
  | Flip -> 
    begin match s.harmonics with
    | Low -> s.harmonics <- High
    | High -> s.harmonics <- Low
    end
  | SMove lld ->
      let c  = bot.coord in
      let c' = c +: lld in
      validate_coord s c';
      validate_region_is_void s (region c c');
      bot.coord <- c';
      s.energy.e_move <- s.energy.e_move + (2 * mlen lld)
  | LMove (sld1, sld2) ->
      let c   = bot.coord in
      let c'  = c +: sld1 in
      let c'' = c' +: sld2 in
      validate_coord s c';
      validate_coord s c'';
      validate_region_is_void s (region c c');
      validate_region_is_void s (region c' c'');
      bot.coord <- c'';
      s.energy.e_move <- s.energy.e_move + 2 * (mlen sld1 + 2 + mlen sld2)
  | Fission (nd, m) -> 
      if bot.seeds = [] then
        raise (BadTrace "no seeds left") else
      let c  = bot.coord in
      let c' = c +: nd in
      let seeds = List.sort ~compare:(fun (BotId b1) (BotId b2) -> Int.compare b1 b2) bot.seeds in
      validate_coord s c';
      validate_coord_is_void s c';
      if List.length seeds < m + 1 then
        raise (BadTrace (Printf.sprintf "Not enough seeds for Bot %s at %s: has %d, needs %d" (string_of_botid bot.id) (string_of_coord c) (List.length seeds) (m+1)) ) else
      let bot' = { id = List.hd_exn seeds; coord = c'; seeds = List.take seeds (m+1) |> List.tl_exn } in
      bot.seeds <- List.drop bot.seeds (m + 1);
      s.bots <- bot'::s.bots;
      s.energy.e_other <- s.energy.e_other + 24
      (* ; Printf.printf "Fission Bot %s at %s: kept %d seeds, gave %d to Bot %s" (string_of_botid bot.id) (string_of_coord c) (List.length bot.seeds) m (string_of_botid bot'.id) *)
  | Fill nd -> 
      let c  = bot.coord in
      let c' = c +: nd in
      validate_coord s c';
      validate_coord_is_void s c;
      validate_coord_is_void s c'; (* For our own sanity. Not needed. *)
      if is_filled s.matrix c' then
        s.energy.e_fill <- s.energy.e_fill + 6
      else begin
        fill s.matrix c';
        s.energy.e_fill <- s.energy.e_fill + 12
      end
  | Void nd ->
      let c  = bot.coord in
      let c' = c +: nd in
      validate_coord s c';
      validate_coord_is_void s c;
      if is_filled s.matrix c' then begin
        void s.matrix c';
        s.energy.e_fill <- s.energy.e_fill - 12
      end else
        s.energy.e_fill <- s.energy.e_fill + 3
  | FusionP _ | FusionS _ | GFill _ | GVoid _ ->
      failwith "not a singleton command"

let step_cmd_group (s : state) (l : cmd_group) = match l with
  | Singleton (cmd, bot) -> step_singleton_cmd s cmd bot
  | FusionPair { primary = (ndp, botp); secondary = (nds, bots); } ->
      (* Printf.printf "fusing pairs (%s, %s) (%s, %s)\n" (string_of_diff ndp) (string_of_bot botp) (string_of_diff nds) (string_of_bot bots); *)
      validate_coord s (botp.coord +: ndp);
      validate_coord s (bots.coord +: nds);
      s.bots <- List.filter ~f:(fun bot -> bot.id <> bots.id) s.bots;
      botp.seeds <- bots.id :: bots.seeds @ botp.seeds;
      s.energy.e_other <- s.energy.e_other - 24
  | GroupRegion { region=r; bots=_; operation } ->
      begin
        let Region (c1, c2) = r in
        validate_coord s c1;
        validate_coord s c2;
      end;
      begin match operation with
      | `Fill ->
          coords_of_region r
          |> OSeq.iter (fun c' ->
              if is_filled s.matrix c' then
                s.energy.e_fill <- s.energy.e_fill + 6
              else begin
                fill s.matrix c';
                s.energy.e_fill <- s.energy.e_fill + 12
              end
            )
      | `Void ->
          coords_of_region r
          |> OSeq.iter (fun c' ->
              if is_filled s.matrix c' then begin
                void s.matrix c';
                s.energy.e_fill <- s.energy.e_fill - 12
              end else
                s.energy.e_fill <- s.energy.e_fill + 3
            )
      end

let sort_bots =
  List.sort ~compare:(fun { id = BotId b1; _ } { id = BotId b2; _ } -> Int.compare b1 b2)

(* external interface *)
let validate_well_formed ~check_grounding (s : state) : unit =
  let validate_all_grounded () =
    (* Do a breadth-first search, starting from y=0 *)
    let { matrix = Matrix (r, _); _ } = s in
    let visited = Model.new_matrix r in
    (* base case: filled voxels with y=0 *)
    let base_grounded =
      List.range 0 r
      |> List.concat_map ~f:(fun x ->
          List.range 0 r
          |> List.map ~f:(fun z -> coord x 0 z)
        )
      |> List.filter ~f:(Model.is_filled s.matrix)
    in
    let step delta =
      List.filter delta ~f:(fun coord ->
        match Model.is_filled visited coord with
        | true -> false
        | false -> Model.fill visited coord; true
      )
      |> List.concat_map ~f:(adjacent_coords ~r)
      |> List.filter ~f:(Model.is_filled s.matrix)
    in
    let rec iter delta =
      match step delta with
      | [] -> ()
      | next -> iter next
    in
    iter base_grounded;
    let grounded_voxel_count = Model.count_voxels visited in
    let all_voxel_count = Model.count_voxels s.matrix in
    if grounded_voxel_count <> all_voxel_count then begin
      raise (BadTrace (
        sprintf "State not well-formed: only %d of %d voxels grounded"
          grounded_voxel_count all_voxel_count))
    end
  in
  (* If the harmonics is Low, then all Full voxels of the matrix are grounded.
   *)
  if check_grounding && s.harmonics = Low then begin
    validate_all_grounded ()
  end;
  (* Each active nanobot has a different identifier. *)
  (* The seeds of each active nanobot does not include the identifier of any
   * active nanobot. *)
  begin
    let ids = Int.Hash_set.create () in
    List.iter s.bots ~f:(fun {id = BotId id; _} ->
      match Hash_set.strict_add ids id with
      | Ok () -> ()
      | Error _ -> raise (BadTrace ("Duplicate bot id "^ string_of_int id))
    );
    List.iter s.bots ~f:(fun {id = BotId id; seeds; _} ->
      List.iter seeds ~f:(fun (BotId seed) ->
        if Hash_set.mem ids seed then begin
          raise (BadTrace (
            sprintf "Bot %d has seed %d, which is an active bot" id seed
          ))
        end
      )
    )
  end;
  (* The position of each active nanobot is distinct and is Void in the
   * matrix. *)
  begin
    let coords = Coord.Table.create () in
    List.iter s.bots ~f:(fun {id = BotId id; coord; _} ->
      if Model.is_filled s.matrix coord then begin
        raise (BadTrace (
          sprintf "Bot %d is in a Full coordinate" id
        ))
      end;
      Hashtbl.change coords coord ~f:(function
        | None -> Some id
        | Some prev_id ->
            raise (BadTrace (
              sprintf "Bots %d and %d are in the same coordinate" prev_id id
            ))
        )
    )
  end;
  (* The seeds of each active nanobot are disjoint. *)
  begin
    let seed_tbl = Int.Table.create () in
    List.iter s.bots ~f:(fun {id = BotId id; seeds; _} ->
      List.iter seeds ~f:(fun (BotId seed) ->
        Hashtbl.change seed_tbl seed ~f:(function
          | None -> Some id
          | Some prev_id ->
              raise (BadTrace (
                sprintf "Bots %d and %d share seed %d" prev_id id seed
              ))
          )
      )
    )
  end;
  ()

let initial_bot =
  { id = BotId 1; coord = coord 0 0 0; seeds = List.range 2 41 |> List.map ~f:(fun x -> BotId x) }

let initial_state (src : matrix) (dst : matrix) (t : command Sequence.t) : state = 
  {
    goal = dst;
    energy = {
      e_time = 0; e_bots = 0; e_fill = 0; e_move = 0; e_other = 0;
    };
    harmonics = Low;
    matrix = src;
    bots = [initial_bot];
    trace = t;
  }

let step (s : state) : unit = 
  let Matrix (r, _) = s.goal in
  let n = List.length s.bots in

  let (trace_head, trace_tail) = Sequence.split_n s.trace n in
  let paired_cmds = List.zip_exn trace_head (sort_bots s.bots) in
  let grouped_cmds = group_cmds paired_cmds in
  validate_interference grouped_cmds;
  let harmonic_energy = (r * r * r * (match s.harmonics with Low -> 3 | High -> 30)) in
  s.energy.e_time <- s.energy.e_time + harmonic_energy;
  s.energy.e_bots <- s.energy.e_bots + 20 * n;
  List.iter ~f:(step_cmd_group s) grouped_cmds;
  s.trace <- trace_tail

let is_done (s : state) : [`NotDone | `IncorrectModel | `CorrectModel] = 
  if s.bots <> [] then `NotDone else
  if same_matrix s.goal s.matrix then `CorrectModel else `IncorrectModel

let total_energy { e_time; e_bots; e_fill; e_move; e_other } =
  e_time + e_bots + e_fill + e_move + e_other

let print_state state round print_matrix =
  let Matrix (r, _) = state.goal in
  let bots = List.map ~f:string_of_bot state.bots |> string_of_list in
  Printf.printf "Round: %d\n" round;
  Printf.printf "  Energy: %14d\n" (total_energy state.energy);
  begin
    let { e_time; e_bots; e_fill; e_move; e_other } = state.energy in
    Printf.printf "    Time (harmonics): %14d\n" e_time;
    Printf.printf "    Active bots:      %14d\n" e_bots;
    Printf.printf "    Moving:           %14d\n" e_move;
    Printf.printf "    Filling:          %14d\n" e_fill;
    Printf.printf "    Other:            %14d\n" e_other;
  end;
  Printf.printf "  Bots: %s\n" bots;
  if print_matrix then 
    for y = 0 to r-1 do 
      Printf.printf "y = %d\n" y;
      print_layer state.matrix y
    done

let run_sim state =
  let rec run round =
    validate_well_formed ~check_grounding:false state;
    step state;
    match is_done state with
    | `CorrectModel ->
        (* print_state state round false; *)
        Some (total_energy state.energy)
    | `IncorrectModel -> None
    | `NotDone -> run (round + 1)
  in run 0
  (* in try run 0 with _ -> None *)

let%test "iter_diff" =
  let coord = coord 10 20 30 in
  let diff = lldiff_y 3 in
  let coords_rev = ref [] in
  iter_diff coord diff ~f:(fun coord ->
    coords_rev := coord :: !coords_rev
  );
  match List.rev !coords_rev with
  | [ Coord (10, 20, 30);  Coord (10, 21, 30); Coord (10, 22, 30) ] -> true
  | _ -> false

let%test "validate_interference" =
  let bot1 = { id = BotId 1;
              coord = coord 2 0 0;
              seeds = [] }
  in
  let bot2 = { id = BotId 2;
              coord = coord 0 1 0;
              seeds = [] }
  in
  validate_interference [
    Singleton (SMove (lldiff_x 10), bot1);
  ];
  validate_interference [
    Singleton (SMove (lldiff_x 10), bot1);
    Singleton (SMove (lldiff_x 10), bot2);
  ];
  begin match
    validate_interference [
      Singleton (SMove (lldiff_y 10), bot1);
      Singleton (SMove (lldiff_x 10), bot2);
    ];
  with
  | exception BadTrace _ -> ()
  | () -> failwith "No exception raised"
  end;
  true

let%test "group commands" =
  let gen_bot id c = { id = BotId id; coord = c; seeds = [] } in
  let c = origin in
  let ndp, botp = ndiff_x 1, gen_bot 1 c in
  let nds, bots = ndiff_x (-1), gen_bot 2 (c +: ndp) in
  let both = gen_bot 3 (coord 1 1 1) in
  let nd, m, botf = ndiff_x 1, 4, gen_bot 4 (coord 2 2 2) in
  let grouped_cmds =
    group_cmds [
      (Wait, both);
      (FusionP ndp, botp);
      (FusionS nds, bots);
      (Fission (nd, m), botf);
    ]
  in
  grouped_cmds = [
    Singleton (Wait, both);
    Singleton (Fission (nd, m), botf);
    FusionPair { primary = (ndp, botp); secondary = (nds, bots) };
  ]

let%test "group_cmds: GFill" =
  (*
   * Fills the following 4x1x2 rectangle in the y=0 plane, where each number
   * on the plane is a bot id:
   *
   *   z
   *   ^.3....
   *   |.###2.
   *   |.###..
   *   |1.4...
   *   +--------> x
   *
   * Simultaneously fills the following 1x3x1 vertical line segment in the z=1
   * plane:
   *
   *   y
   *   ^......
   *   |......
   *   |..#6..
   *   |..#5..
   *   |..#...
   *   |!!!!!!  <-- this is where the rectangle is being created
   *   +--------> x
   *)
  let gen_bot id c = { id = BotId id; coord = c; seeds = [] } in
  let bot1 = gen_bot 1 (coord 0 0 0) in
  let bot2 = gen_bot 2 (coord 4 0 2) in
  let bot3 = gen_bot 3 (coord 1 0 3) in
  let bot4 = gen_bot 4 (coord 2 0 0) in
  let nd1 = ndiff_xz 1 1 in
  let nd2 = ndiff_x (-1) in
  let nd3 = ndiff_z (-1) in
  let nd4 = ndiff_xz 1 1 in
  let fd1 = fdiff_of_triple (2, 0, 1) in
  let fd2 = fdiff_of_triple (-2, 0, -1) in
  let fd3 = fdiff_of_triple (2, 0, -1) in
  let fd4 = fdiff_of_triple (-2, 0, 1) in
  let bot5 = gen_bot 5 (coord 3 2 1) in
  let bot6 = gen_bot 6 (coord 3 3 1) in
  let nd5 = ndiff_xy (-1) (-1) in
  let nd6 = ndiff_x (-1) in
  let fd5 = fdiff_of_triple (0, 2, 0) in
  let fd6 = fdiff_of_triple (0, -2, 0) in
  let bot_h = gen_bot 20 (coord 1 1 1) in
  let grouped_cmds =
    group_cmds [
      Wait, bot_h;
      GFill (nd5, fd5), bot5;
      GFill (nd1, fd1), bot1;
      GFill (nd2, fd2), bot2;
      GFill (nd3, fd3), bot3;
      GFill (nd4, fd4), bot4;
      GFill (nd6, fd6), bot6;
    ]
  in
  match grouped_cmds with
  | [
      Singleton (Wait, _);
      GroupRegion { region = line; operation = `Fill; bots=bots_line; };
      GroupRegion { region = rect; operation = `Fill; bots=bots_rect; };
    ] ->
      rect = region (coord 1 0 1) (coord 3 0 2) &&
      line = region (coord 2 1 1) (coord 2 3 1) &&
      let compare_nanobot bot1 bot2 =
        compare_botid bot1.id bot2.id
      in
      List.sort ~compare:compare_nanobot bots_rect =
        [bot1; bot2; bot3; bot4] &&
      List.sort ~compare:compare_nanobot bots_line = [bot5; bot6]
  | _ -> false

let%test "group_cmds: GVoid" =
  (*
   * Voids the following 4x1x2 rectangle in the y=0 plane, where each number
   * on the plane is a bot id:
   *
   *   z
   *   ^.3....
   *   |.###2.
   *   |.###..
   *   |1.4...
   *   +--------> x
   *
   * Simultaneously voids the following 1x3x1 vertical line segment in the z=1
   * plane:
   *
   *   y
   *   ^......
   *   |......
   *   |..#6..
   *   |..#5..
   *   |..#...
   *   |!!!!!!  <-- this is where the rectangle is being created
   *   +--------> x
   *)
  let gen_bot id c = { id = BotId id; coord = c; seeds = [] } in
  let bot1 = gen_bot 1 (coord 0 0 0) in
  let bot2 = gen_bot 2 (coord 4 0 2) in
  let bot3 = gen_bot 3 (coord 1 0 3) in
  let bot4 = gen_bot 4 (coord 2 0 0) in
  let nd1 = ndiff_xz 1 1 in
  let nd2 = ndiff_x (-1) in
  let nd3 = ndiff_z (-1) in
  let nd4 = ndiff_xz 1 1 in
  let fd1 = fdiff_of_triple (2, 0, 1) in
  let fd2 = fdiff_of_triple (-2, 0, -1) in
  let fd3 = fdiff_of_triple (2, 0, -1) in
  let fd4 = fdiff_of_triple (-2, 0, 1) in
  let bot5 = gen_bot 5 (coord 3 2 1) in
  let bot6 = gen_bot 6 (coord 3 3 1) in
  let nd5 = ndiff_xy (-1) (-1) in
  let nd6 = ndiff_x (-1) in
  let fd5 = fdiff_of_triple (0, 2, 0) in
  let fd6 = fdiff_of_triple (0, -2, 0) in
  let bot_h = gen_bot 20 (coord 1 1 1) in
  let grouped_cmds =
    group_cmds [
      Wait, bot_h;
      GVoid (nd5, fd5), bot5;
      GVoid (nd1, fd1), bot1;
      GVoid (nd2, fd2), bot2;
      GVoid (nd3, fd3), bot3;
      GVoid (nd4, fd4), bot4;
      GVoid (nd6, fd6), bot6;
    ]
  in
  match grouped_cmds with
  | [
      Singleton (Wait, _);
      GroupRegion { region = line; operation = `Void; bots=bots_line; };
      GroupRegion { region = rect; operation = `Void; bots=bots_rect; };
    ] ->
      rect = region (coord 1 0 1) (coord 3 0 2) &&
      line = region (coord 2 1 1) (coord 2 3 1) &&
      let compare_nanobot bot1 bot2 =
        compare_botid bot1.id bot2.id
      in
      List.sort ~compare:compare_nanobot bots_rect =
        [bot1; bot2; bot3; bot4] &&
      List.sort ~compare:compare_nanobot bots_line = [bot5; bot6]
  | _ -> false
