open Core
open Types
open MicroLang
open TracerHelpers

type rect_spec = {
  lower_corner_z: int;
  bot_spacing_z: int; (* if it's 0, that means it's a line between two bots. TODO: needed? *)
}

type grid_spec = {
  rects: rect_spec list;
  bot_spacing_x: int; (** difference between x coords of bot pairs; up to 30 *)
  x_moves: int list; (* each item is up to 30. May be empty. *)
}

(* Returns the grid layout.
 *
 *   z
 *   ^
 *   |
 *  60* . . * . . . .
 *  59* . . * . . . .
 *   |. . . . . . . .
 *   |. . . . . . . .
 *  30* . . * . . . .
 *  29* . . * . . . .
 *   |. . . . . . . .
 *   |. . . . . . . .
 *   0* . . * . . . .
 *   +0----29----------> x
 *
 * And how many times the voiding should be done on the x axis
 *)
let make_grid_spec ~x_width ~z_depth : grid_spec =
  (* We require a minimal x thickness. This could be a problem if we use this
   * algorithm in a thin slice. *)
  assert (x_width >= 2 && z_depth >= 1);
  let whole_squares = z_depth / 30 in
  let last_square_zheight = z_depth - 30 * whole_squares in
  let square_xwidth = Int.min x_width 30 in
  let moves_needed = (x_width - 1) / 30 in
  assert false

let entire_model (Matrix (r,_) : Model.matrix) =
  (region (coord 0 0 0) (coord (r-2) (r-2) (r-2)))

let everything (m: Model.matrix) =
  let bounding_box = Model.optimize_region m (entire_model m) in
  let (bb_x_width, bb_y_height, bb_z_depth) = region_size bounding_box in
  let spec = make_grid_spec ~x_width:bb_x_width ~z_depth:bb_z_depth in
  let (spread_out, min_corner) =
    (* Move to top of figure bounding box. *)
    let (move_to_top_corner, cur_pos) =
      let Region (Coord (xmin, _, zmin), _) = bounding_box in
      ([], (coord 0 0 0)) >>:
      trace_linear_move (ldiff_y bb_y_height) >>:
      trace_linear_move (ldiff_x xmin) >>:
      trace_linear_move (ldiff_z zmin)
    in
    (* Make one copy on the right and move it into place. *)
    (* TODO: Handle special case of bot_spacing_x = 0 ONLY IF NEEDED BY AN ACTUAL
    * MAP.*)
    let spread_along_x =
      Array.append
        [| [Fission (cur_pos, ndiff_x 1, 40/2)] |]
        ((
          ([], cur_pos +: ndiff_x 1) >>:
          trace_linear_move (ldiff_x (spec.bot_spacing_x - 1))
        ) |> fst |> microtrace_of_singular_trace)
    in
    (* Then, `pool_me` for both copies along z, merging the traces. *)
    let pool_trace =
      let Coord (_, _, cur_z) = cur_pos in
      let pool_spec =
        List.concat_map spec.rects ~f:(fun rect ->
          [ cur_z + rect.lower_corner_z;
            cur_z + rect.lower_corner_z + rect.bot_spacing_z ]
        )
        |> List.map ~f:(fun z -> (z, 0))
      in
      let trace1 = InitPooler.pool_me `Z cur_pos pool_spec in
      let trace2 =
        MicroLangHelper.shift_microtrace (ldiff_x spec.bot_spacing_x) trace1
      in
      MicroLangHelper.weave_two_traces_exn trace1 trace2
    in
    (Array.append (microtrace_of_singular_trace move_to_top_corner)
                  (Array.append spread_along_x pool_trace),
     cur_pos)
  in
  (* TODO: should spec be mapped into the bb's absolute coords? *)
  (* For each y layer: *)
    (* For each voiding, do GVoid and Move in alternation (reverse in odd
     * layers).
     * All GVoid commands are the same, execpt for the coordinate. *)
  let initial_corner_pos ~y =
    assert false
  in
  let rec loop_y ~y ~acc =
    match y with
    | -1 -> List.rev acc
    | _ ->
        let do_void_rectangle ~x_corner =
          assert false
        in
        (* Here, `moves_rev` is a reversed list that, for each _move_ (in the
         * sense of `spec.x_moves`) has the commands to move the corner bot. *)
        let (moves_rev, rightmost_corner_pos) =
          List.fold spec.x_moves ~init:([], initial_corner_pos ~y)
                                 ~f:(fun (moves_rev, pos) dx ->
            let (steps, next_pos) =
              ([], pos) >>:
              trace_linear_move (ldiff_x dx)
            in
            (steps :: moves_rev, next_pos)
          )
        in
        let (move_back, _) =
          let dist =
            List.sum (module Int) ~f:Fn.id spec.x_moves
          in
          ([], rightmost_corner_pos) >>:
          trace_linear_move (ldiff_x (-dist))
        in
        let moves_and_move_back = List.rev_append moves_rev [ move_back ] in
        assert false
(*
        let do_layer =
          List.intersperse
        let loop_x ~x_corner ~acc =
          let is_final_voiding
          let cmds =
            Array.append
              (do_void_rectangles ~x_corner)
              (Array.
            List.map spec.rects ~f:(fun rect ->
*)
        in
  assert false
  (* Move all bots one layer down. *)
  (* Move back to corner if we did an odd number of layers *)
  (* Finally, shift and invert the initial move dance, including splitting. *)
