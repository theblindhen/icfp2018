open Core
open Types
open TracerHelpers
open MicroLang
      
let strategy (m : Model.matrix) =
  let Matrix (r,_) = m in
  let cur_pos = coord 0 0 0 in
  let model_region =
    region cur_pos (coord (r-2) (r-2) (r-2))
    (* |> Model.optimize_region m *)
  in
  let Region (Coord (x1, y1, z1),_) = model_region in
  assert (y1 = 0);
  let cmds, _ =
    ([], cur_pos)
    >>: trace_linear_move (ldiff_x x1)
    >>: trace_linear_move (ldiff_z z1)
    >>: CleverFill.trace_region_in_layer m model_region
    >>: (fun (Coord (x,y,z) as cur_pos) ->
            ([], cur_pos)
            >>: trace_linear_move (ldiff_z (-z))
            >>: trace_linear_move (ldiff_x (-x))
            >>: trace_linear_move (ldiff_y (-y)))
  in
  microtrace_of_singular_trace cmds
