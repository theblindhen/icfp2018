open Core
open Types
open MicroLang
open TracerHelpers
open PrettyPrinters
   
(* The following was old trace_region code
 * ===
 * (\* Traces from cur_pos to cur_pos + diff, both positions inclusive *\)
 * let trace_linear_fill (m : Model.matrix) (fill_diff : ndiff) (Diff (dx,dy,dz) as diff : ldiff) (cur_pos : coord) =
 *   if dx = 0 && dy = 0 && dz = 0 then
 *     (\* We're actually not moving at all, so just check at the cur_pos *\)
 *     ([], cur_pos)
 *     >>: trace_cond_fill m fill_diff
 *   else
 *     let mover, dmove =
 *         match dx <> 0 , dy <> 0 , dz <> 0 with
 *         | true, false, false -> lldiff_x , if dx < 0 then -1 else 1
 *         | false, true, false -> lldiff_y , if dy < 0 then -1 else 1
 *         | false, false, true -> lldiff_z , if dz < 0 then -1 else 1
 *         | _ -> failwith "trace_linear_diff: Not a linear diff"
 *     in
 *     let check_first = ([], cur_pos) >>: trace_cond_fill m fill_diff in
 *     List.range 0 (clen diff)
 *     |> List.fold ~init:check_first
 *         ~f:(fun acc _ ->
 *                 acc >>: trace_smove (mover dmove)
 *                     >>: trace_cond_fill m fill_diff)
 * 
 * (\* Trace the given region and return the final position.
 *    Assumes that the bot is already in the lower-left-near corner.
 *    The bot will not move outside the region except for the last move.
 *    The final position might be at one y-value higher than the region.
 *  *\)
 * let trace_region (m : Model.matrix) (region : region) (cur_pos : coord) =
 *   let Region (Coord (rx1,_,rz1) as r1, Coord (rx2,ry2,rz2)) = region in
 *   assert (cur_pos = r1);
 *   let rec fill_rows (left_to_right : bool) (Coord (x,_,z) as cur_pos : coord) =
 *     (\* fill the current row i.e. x and y coords set, move along z *\)
 *     (\* cur_pos.z must be either rz1 or rz2 *\)
 *     (\* Like trace_region, this will end up at the y-value above the current, in one of the corners *\)
 *     let at_near = z = rz1 in
 *     let move_dx = if left_to_right then  1 else -1 in
 *     let diffz = if at_near then rz2 - rz1 else rz1 - rz2 in
 *     if (left_to_right && x = rx2) || (not left_to_right && x = rx1) then
 *       (\* For the last row, we fill behind us and then move one up *\)
 *       let move_dz = if diffz > 0 then 1 else -1 in
 *       ([], cur_pos)
 *       >>: trace_smove (lldiff_z move_dz)
 *       >>: trace_linear_fill m (ndiff_z (-move_dz)) (ldiff_z (diffz-move_dz))
 *       >>: trace_smove (lldiff_y 1)
 *       >>: trace_cond_fill m (ndiff_y (-1))
 *     else
 *       (\* For intermediate rows, move along z, filling to the left or right *\)
 *       let fill_diff = ndiff_x (if left_to_right then -1 else 1) in
 *       ([], cur_pos)
 *       >>: trace_smove (lldiff_x move_dx)
 *       >>: trace_linear_fill m fill_diff (ldiff_z diffz)
 *       >>: fill_rows left_to_right
 *   in
 *   let rec fill_layers (Coord (x,y,_) as cur_pos : coord) =
 *     if y = ry2+1 then
 *       ([], cur_pos)
 *     else
 *       ([], cur_pos)
 *       >>: fill_rows (x = rx1)
 *       >>: fill_layers
 *   in
 *   ([], cur_pos)
 *   >>: fill_layers *)

let voxels_in_fill_order (region : region) =
  let Region (Coord (rx1,ry1,rz1), Coord (rx2,ry2,rz2)) = region in
  (* Printf.printf "vox %s\n" (string_of_region region); *)
  let targets = ref [] in
  for y=ry1 to ry2 do
    for dx=0 to rx2-rx1 do
      let x = if y%2 = 0 then rx1 + dx else rx2 - dx in
      for dz=0 to rz2-rz1 do
        let z = if (y + x) % 2 = 0 then rz1 + dz else rz2 - dz in
        targets := coord x y z :: !targets
      done
    done
  done;
  !targets |> List.rev

let trace_region_above_lazy (m : Model.matrix) (region : region) =
  let Region (cur_pos,_) = region in
  (* Printf.printf "trace region %s from %s" (string_of_region region) (string_of_coord cur_pos); *)
  let fill_targets =
    voxels_in_fill_order region
    |> List.filter ~f:(Model.is_filled m)
  in
  let (cmdssr, end_pos) =
    List.fold fill_targets ~init:([], cur_pos)
      ~f:(fun (cmdssr, cur_pos) (Coord (tx,ty,tz) as target_pos) ->
        let Coord (x,y,z) = cur_pos in
        let (cmds, end_pos) = 
          if y = ty+1
             && is_ndiff (target_pos -: cur_pos) then
            (* TODO: Should also look for other neighbors which are targets
               inside region, and not just the next target *)
            trace_fill (ndiff_of_diff (target_pos -: cur_pos)) cur_pos
          else
            ([], cur_pos)
            >>: trace_linear_move (ldiff_y (ty - y + 1))
            >>: trace_linear_move (ldiff_x (tx - x))
            >>: trace_linear_move (ldiff_z (tz - z))
            >>: (fun cur_pos ->
                let fill_diff = ndiff_of_diff (target_pos -: cur_pos) in
                trace_fill fill_diff cur_pos)
        in
        (cmds::cmdssr, end_pos)
      )
  in
  (cmdssr |> List.rev |> List.concat, end_pos)
  >>: (fun end_pos -> trace_diff_move_xzy ((region_pos_out region) -: end_pos) end_pos)

let trace_region_above_eager (Matrix (r,_) as m : Model.matrix) (region : region) =
  let Region (Coord (rx1,_,_) as cur_pos, Coord (rx2,_,_)) = region in
  (* Printf.printf "Region at %s , region %s\n" (string_of_coord cur_pos) (string_of_region region); *)
  let fill_targets =
    voxels_in_fill_order region
    |> List.filter ~f:(Model.is_filled m) in
  let already_filled = Coord.Table.create () in
  let was_filled pos =
    match Coord.Table.find already_filled pos with
    | None -> false
    | Some () -> true
  in
  let my_trace_fill fill_diff cur_pos =
    Coord.Table.set already_filled ~key:(cur_pos +: fill_diff) ~data:();
    (* Printf.printf "Filling %s\n" (string_of_coord (cur_pos +: fill_diff)); *)
    trace_fill fill_diff cur_pos
  in
  let (cmdssr, end_pos) =
    List.fold fill_targets ~init:([], cur_pos)
      ~f:(fun (cmdssr, cur_pos) (Coord (tx,ty,tz) as target_pos) ->
        if was_filled target_pos then
          (cmdssr, cur_pos)
        else
          let Coord (x,y,z) = cur_pos in
          let left_to_right = ty % 2 = 0 in
          let goto_x = Int.max rx1 (Int.min rx2 (tx + (if left_to_right then 1 else -1))) in
          (* Printf.printf "%d-%d : %d\n" rx1 rx2 goto_x ; *)
          let (cmds, end_pos) = 
            ([], cur_pos)
            >>: trace_linear_move (ldiff_y (ty - y + 1))
            >>: trace_linear_move (ldiff_x (goto_x - x))
            >>: trace_linear_move (ldiff_z (tz - z))
            >>: (fun cur_pos ->
              let fill_diff = ndiff_of_diff (target_pos -: cur_pos) in
              my_trace_fill fill_diff cur_pos)
            >>: (fun cur_pos ->
              adjacent_coords ~r:r cur_pos
              |> List.filter ~f:(fun (Coord (_,ny,_)) -> ny < y)
              |> List.filter ~f:(in_region region)
              |> List.filter ~f:(fun neighbor ->
                     (not (was_filled neighbor)) && Model.is_filled m neighbor)
              |> List.fold ~init:([], cur_pos) ~f:(fun state neighbor ->
                     state >>: my_trace_fill (ndiff_of_diff (neighbor -: cur_pos))))
          in
          (cmds::cmdssr, end_pos)
      )
  in
  (cmdssr |> List.rev |> List.concat, end_pos)

(* TODO: This is buggy and actually leaves the region on the x-coord *)
let trace_region_in_layer (m : Model.matrix) (region : region) (cur_pos : coord) =
  let Region (r1,_) = region in
  assert (cur_pos = r1);
  let fill_targets =
    voxels_in_fill_order region
    |> List.filter ~f:(Model.is_filled m)
  in
  let (cmdssr, end_pos) =
    List.fold fill_targets ~init:([], cur_pos)
      ~f:(fun (cmdssr, cur_pos) (Coord (tx,ty,tz) as target_pos) ->
        let Coord (x,y,z) = cur_pos in
        let left_to_right = ty % 2 = 0 in
        let fill_dx = if left_to_right then -1 else 1 in
        let (cmds, end_pos) = 
          if y = ty
             && is_ndiff (target_pos -: cur_pos)
             && (if left_to_right then tx <= x else tx >= x) then
            (* If possible from here, it is now always safe to place the next one *)
            trace_fill (ndiff_of_diff (target_pos -: cur_pos)) cur_pos
          else
            ([], cur_pos)
            >>: trace_linear_move (ldiff_y (ty - y))
            >>: trace_linear_move (ldiff_x (tx - x - fill_dx))
            >>: trace_linear_move (ldiff_z (tz - z))
            >>: (fun cur_pos ->
                let fill_diff = ndiff_x fill_dx in
                assert (cur_pos +: fill_diff = target_pos);
                trace_fill fill_diff cur_pos)
        in
        (cmds::cmdssr, end_pos)
      )
  in
  (cmdssr |> List.rev |> List.concat, end_pos)
  >>: trace_smove (lldiff_y 1)

let skip_if cond f =
  fun a -> if cond then ([], a) else f(a)
