open Core

type coord = Coord of int * int * int
  [@@deriving compare, hash, sexp]

(* Boilerplate to let us put `coord` in hash tables *)
module Coord = struct
  module T = struct
    type t = coord
    let hash = hash_coord
    let sexp_of_t = sexp_of_coord
    let t_of_sexp = coord_of_sexp
    let compare = compare_coord
    let hash_fold_t = hash_fold_coord
  end
  include T
  include Hashable.Make(T)
  include Comparable.Make (T)
end

type +'a gen_diff = Diff of int * int * int

type diff = [`any] gen_diff

type ldiff = [`l|`any] gen_diff
type lldiff = [`ll|`l|`f|`any] gen_diff
type sldiff = [`sl|`ll|`l|`f|`any] gen_diff
type ndiff = [`n|`f|`any] gen_diff
type fdiff = [`f|`any] gen_diff

let coord x y z =
  assert (x >= 0 && y >= 0 && z >= 0);
  Coord(x, y, z)

let origin = Coord(0, 0, 0)

let string_of_coord (Coord (x, y, z)) =
  sprintf "(%d, %d, %d)" x y z

let (-:) (Coord(x1, y1, z1)) (Coord(x2, y2, z2)) =
  Diff(x1 - x2, y1 - y2, z1 - z2)

let (+:) (Coord(x, y, z)) (Diff(dx, dy, dz)) =
  coord (x + dx) (y + dy) (z + dz)

let ( *:) (c : int) (Diff (dx, dy, dz)) =
  Diff(c*dx, c*dy, c*dz)

let invert_diff (Diff (dx, dy, dz)) =
  Diff (-dx, -dy, -dz)

let transform_coord f (Coord (x, y, z)) = 
  let (fx, fy, fz) = f (x, y, z) in Coord(fx, fy, fz)

let transform_diff f (Diff (dx, dy, dz)) =
  let (fdx, fdy, fdz) = f (dx, dy, dz) in Diff(fdx, fdy, fdz)

let ndiff_x dx =
  assert (abs dx = 1);
  Diff (dx, 0, 0)

let ndiff_y dy =
  assert (abs dy = 1);
  Diff (0, dy, 0)

let ndiff_z dz =
  assert (abs dz = 1);
  Diff (0, 0, dz)

let ndiff_xy dx dy =
  assert (abs dx = 1 && abs dy = 1);
  Diff (dx, dy, 0)

let ndiff_xz dx dz =
  assert (abs dx = 1 && abs dz = 1);
  Diff (dx, 0, dz)

let ndiff_yz dy dz =
  assert (abs dy = 1 && abs dz = 1);
  Diff (0, dy, dz)

let sldiff_x dx =
  assert (abs dx <= 5 && dx <> 0);
  Diff (dx, 0, 0)

let sldiff_y dy =
  assert (abs dy <= 5 && dy <> 0);
  Diff (0, dy, 0)

let sldiff_z dz =
  assert (abs dz <= 5 && dz <> 0);
  Diff (0, 0, dz)

let lldiff_x dx =
  assert (abs dx <= 15 && dx <> 0);
  Diff (dx, 0, 0)

let lldiff_y dy =
  assert (abs dy <= 15 && dy <> 0);
  Diff (0, dy, 0)

let lldiff_z dz =
  assert (abs dz <= 15 && dz <> 0);
  Diff (0, 0, dz)

let ldiff_x dx = Diff (dx, 0, 0)

let ldiff_y dy = Diff (0, dy, 0)

let ldiff_z dz = Diff (0, 0, dz)

let direction_of_diff (Diff (dx, dy, dz)) =
  Diff (Sign.to_int (Int.sign dx),
        Sign.to_int (Int.sign dy),
        Sign.to_int (Int.sign dz))

let mlen (Diff (dx, dy, dz)) = (abs dx) + (abs dy) + (abs dz)

let clen (Diff (dx, dy, dz)) = max (abs dx) (max (abs dy) (abs dz))

let is_ldiff (diff : 'a gen_diff) = clen(diff) = mlen(diff)

let is_ndiff (diff : 'a gen_diff) = clen diff = 1 && mlen diff <= 2

let is_sldiff (diff : 'a gen_diff) =
  let len = clen diff in
  is_ldiff diff && len > 0 && len <= 5

let is_lldiff (diff : 'a gen_diff) =
  let len = clen diff in
  is_ldiff diff && len > 0 && len <= 15

let ldiff_of_diff (Diff (dx, dy, dz) as diff) =
  assert (is_ldiff diff);
  Diff (dx, dy, dz)

let lldiff_of_ldiff (Diff (dx, dy, dz) as diff) =
  assert (is_lldiff diff);
  Diff (dx, dy, dz)

let ndiff_of_diff (Diff (dx, dy, dz) as diff) =
  assert (is_ndiff diff);
  Diff (dx, dy, dz)

let sldiff_of_ldiff (Diff (dx, dy, dz) as diff) =
  let len = clen(diff) in
  assert (len > 0 && len <= 5);
  Diff (dx, dy, dz)

let sldiff_of_lldiff (Diff (dx, dy, dz) as diff) =
  let len = clen(diff) in
  assert (len > 0 && len <= 5);
  Diff (dx, dy, dz)

let (@:) f g x = f (g x)
let lldiff_of_diff = lldiff_of_ldiff @: ldiff_of_diff
let sldiff_of_diff = sldiff_of_ldiff @: ldiff_of_diff


let diff_of_triple (dx, dy, dz) = Diff (dx, dy, dz)

let ldiff_of_triple (dx, dy, dz) =
  let diff = Diff (dx, dy, dz) in
  assert (
    let mlen_diff = mlen diff in
    let clen_diff = clen diff in
    mlen_diff = clen_diff && 0 < mlen_diff && mlen_diff <= 15
  );
  diff

let lldiff_of_triple (dx, dy, dz) =
  let diff = Diff (dx, dy, dz) in
  assert (
    let mlen_diff = mlen diff in
    let clen_diff = clen diff in
    mlen_diff = clen_diff && 0 < mlen_diff && mlen_diff <= 15
  );
  diff

let sldiff_of_triple (dx, dy, dz) = 
  let diff = Diff (dx, dy, dz) in
  assert (
    let mlen_diff = mlen diff in
    let clen_diff = clen diff in
    mlen_diff = clen_diff && 0 < mlen_diff && mlen_diff <= 5
  );
  diff

let ndiff_of_triple (dx, dy, dz) = 
  let diff = Diff (dx, dy, dz) in
  assert (
    let mlen_diff = mlen diff in
    let clen_diff = clen diff in
    0 < mlen_diff && mlen_diff <= 2 && clen_diff = 1
  );
  diff

let fdiff_of_triple (dx, dy, dz) =
  let diff = Diff (dx, dy, dz) in
  assert (
    let clen_diff = clen diff in
    0 < clen_diff && clen_diff <= 30
  );
  diff

(** Always in canonical form: smallest coordinate first. *)
type region = Region of coord * coord

let region (Coord(x1, y1, z1)) (Coord(x2, y2, z2)) =
  Region(
    Coord(Int.min x1 x2, Int.min y1 y2, Int.min z1 z2),
    Coord(Int.max x1 x2, Int.max y1 y2, Int.max z1 z2)
  )

let region_dim (Region (Coord (x1, y1, z1), Coord (x2, y2, z2))) =
  Bool.to_int (x1 <> x2) +
  Bool.to_int (y1 <> y2) +
  Bool.to_int (z1 <> z2)

let in_region'
      (Region (Coord (x1, y1, z1), Coord (x2, y2, z2)))
      x y z =
  (* No need for min/max here as a `region` is always canonical *)
  x1 <= x && x <= x2 &&
  y1 <= y && y <= y2 &&
  z1 <= z && z <= z2

let in_region r (Coord (x,y,z)) = in_region' r x y z

let region_size (Region (Coord (x1, y1, z1), Coord (x2, y2, z2))) =
  (x2 - x1, y2 - y1, z2 - z1)

let coords_of_region (Region (Coord (x1, y1, z1), Coord (x2, y2, z2))) =
  OSeq.(
    x1 -- x2 >>= fun x ->
    y1 -- y2 >>= fun y ->
    z1 -- z2 >>= fun z ->
    return (coord x y z)
  )

let adjacent_coords ~r (Coord (x, y, z)) =
  let entire_map = region origin (coord (r-1) (r-1) (r-1)) in
  [
    (x+1, y, z); (x, y+1, z); (x, y, z+1);
    (x-1, y, z); (x, y-1, z); (x, y, z-1);
  ]
  |> List.filter_map ~f:(fun (x, y, z) ->
      if in_region' entire_map x y z then
        Some (Coord (x, y, z))
      else
        None
  )

let opposite_corner (Region (Coord (rx1,ry1,rz1), Coord (rx2,ry2,rz2))) (Coord (cx, cy, cz)) =
  if not (cx = rx1 || cx = rx2) && not (cy = ry1 || cy = ry2) &&  not (cy = ry1 || cy = ry2) then
    failwith "Not a corner of the region";
  let ox = if cx = rx1 then rx2 else rx1 in
  let oy = if cy = ry1 then ry2 else ry1 in
  let oz = if cz = rz1 then rz2 else rz1 in
  coord ox oy oz

let slice_region axis (Region (Coord (rx1,ry1,rz1), Coord (rx2,ry2,rz2))) t1 t2 =
  (* Slice the region along axis from t1 to t2 *)
  match axis with
  | `X -> region (coord t1 ry1 rz1) (coord t2 ry2 rz2)
  | `Y -> region (coord rx1 t1 rz1) (coord rx2 t2 rz2)
  | `Z -> region (coord rx1 ry1 t1) (coord rx2 ry2 t2)

let%test "adjacent_coords" =
  let output = adjacent_coords ~r:2 (coord 0 1 0) in
  output = [ coord 1 1 0; coord 0 1 1; coord 0 0 0 ]
