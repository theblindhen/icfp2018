open Core
open Types
open PrettyPrinters

(*
 * Returns a list of x/z coordinates where bots should be placed. The list is
 * ordered by increasing coordinate. The first element of the result is always
 * the lln-corner of the region, even on an empty map.
 *
 * The optional parameter `~suppress_warning` will suppress a warning to
 * stderr when the algorithm was not smart enough to use the optimal number of
 * bots.
 *)
let divide_along_axis ?suppress_warning matrix (Region (Coord (rx1,ry1,rz1), Coord (rx2,ry2,rz2))) axis nbots : int list =
  let t_to_axis t =
    match axis with
    | `X -> t+rx1
    | `Z -> t+rz1 in
  let tuv_to_xyz t u v =
    match axis with
    | `X -> coord (t+rx1) (u+ry1) (v+rz1)
    | `Z -> coord (u+rx1) (v+ry1) (t+rz1) in
  let tlen, ulen, vlen =
    match axis with
    | `X -> rx2-rx1, ry2-ry1, rz2-rz1
    | `Z -> rz2-rz1, rx2-rx1, ry2-ry1 in
  let volume_per_t =
    OSeq.(0 -- tlen) |> OSeq.map (fun t ->
        OSeq.(0 -- ulen) |> OSeq.flat_map (fun u ->
          OSeq.(0 -- vlen) |> OSeq.map (fun v ->
            Bool.to_int (Model.is_filled matrix (tuv_to_xyz t u v))
          )
        )
        |> OSeq.sum
    )
    |> OSeq.to_array
  in
  (* This returns a reverse list of slice widths *)
  let rec divide_remaining ~acc ~slices_left ~volume_left ~t_coord =
    if volume_left = 0 || slices_left = 0 then
      acc
    else
      let rec advance ~volume_needed ~t_coord =
        let volume_at_t = Array.get volume_per_t t_coord in
        if volume_at_t >= volume_needed && volume_at_t > 0 then
          (t_coord + 1, volume_at_t)
        else
          let (final_t, remaining_volume_taken) =
            advance ~volume_needed:(volume_needed - volume_at_t)
                    ~t_coord:(t_coord + 1)
          in
          (final_t, volume_at_t + remaining_volume_taken)
      in
      (* This division can become 0 due to rounding. *)
      let ideal_slice_volume = volume_left / slices_left in
      let (t_next, delta_volume) =
        advance ~volume_needed:ideal_slice_volume ~t_coord
      in
      divide_remaining ~acc:(t_coord :: acc)
                        ~slices_left:(slices_left - 1)
                        ~volume_left:(volume_left - delta_volume)
                        ~t_coord:t_next
  in
  let num_slices = Int.min (tlen+1) nbots in
  assert (num_slices >= 1);
  let total_volume = OSeq.sum (OSeq.of_array volume_per_t) in
  match
    divide_remaining ~acc:[]
                     ~slices_left:num_slices
                     ~volume_left:total_volume
                     ~t_coord:0
  with
  | [] -> [0] (* special case *)
  | result_rev ->
      (* Warn when imperfect *)
      if suppress_warning = None then begin
        let non_empty_slice_count = Array.count volume_per_t ~f:((<>) 0) in
        let result_length = List.length result_rev in
        let desired_length = Int.min non_empty_slice_count nbots in
        if result_length <> desired_length then
          eprintf "divide_along_axis `X: using only %d of %d bots\n"
            result_length desired_length;
      end;
      List.rev result_rev
      |> List.map ~f:t_to_axis

let entire_model r = (region (coord 0 0 0) (coord (r-2) (r-2) (r-2)))

let%test "divide_along_axis: empty" =
  let empty = Model.new_matrix 5 in
  let result = divide_along_axis empty (entire_model 5) `X 40 in
  result = [0]

let%test "divide_along_axis: one voxel" =
  let matrix = Model.new_matrix 5 in
  Model.fill' matrix (1,0,1);
  let result = divide_along_axis matrix (entire_model 5) `X 40 in
  result = [0]

let%test "divide_along_axis: two voxels along z" =
  let matrix = Model.new_matrix 5 in
  Model.fill' matrix (1,0,1);
  Model.fill' matrix (1,0,3);
  let result = divide_along_axis matrix (entire_model 5) `X 40 in
  result = [0]

let%test "divide_along_axis: two voxels along x" =
  let matrix = Model.new_matrix 5 in
  Model.fill' matrix (1,0,1);
  Model.fill' matrix (3,0,1);
  let result = divide_along_axis matrix (entire_model 5) `X 40 in
  result = [0;2]

let%test "divide_along_axis: three voxels along x" =
  let matrix = Model.new_matrix 5 in
  Model.fill' matrix (1,0,1);
  Model.fill' matrix (2,1,1);
  Model.fill' matrix (3,0,1);
  let result = divide_along_axis matrix (entire_model 5) `X 40 in
  result = [0;2;3]

let%test "divide_along_axis: small solid box" =
  let matrix = Model.new_matrix 8 in
  OSeq.(1 -- 6) |> OSeq.iter (fun x ->
    OSeq.(0 -- 2) |> OSeq.iter (fun y ->
      OSeq.(1 -- 6) |> OSeq.iter (fun z ->
        Model.fill' matrix (x,y,z);
      )
    )
  );
  let result = divide_along_axis matrix (entire_model 8) `X 40 in
  let expected = [ 0; 2; 3; 4; 5; 6 ] in
  result = expected

let%test "divide_along_axis: large solid box" =
  let matrix = Model.new_matrix 53 in
  OSeq.(1 -- 51) |> OSeq.iter (fun z ->
    OSeq.(0 -- 2) |> OSeq.iter (fun y ->
      OSeq.(1 -- 51) |> OSeq.iter (fun x ->
        Model.fill' matrix (x,y,z);
      )
    )
  );
  let result = divide_along_axis matrix (entire_model 53) `Z 40 in
  let expected =
    [ 0; 3; 5; 7; 9; 11; 13; 15; 17; 19; 21; 23; 24; 25; 26; 27; 28; 29; 30; 31;
      32; 33; 34; 35; 36; 37; 38; 39; 40; 41; 42; 43; 44; 45; 46; 47; 48; 49;
      50; 51 ]
  in
  result = expected && List.length expected = 40

let%test "divide_along_axis: large xz triangle, decreasing" =
  let matrix = Model.new_matrix 77 in
  OSeq.(1 -- 75) |> OSeq.iter (fun x ->
    OSeq.(1 -- (76 - x)) |> OSeq.iter (fun z ->
      Model.fill' matrix (x,0,z);
    )
  );
  let result = divide_along_axis matrix (entire_model 77) `Z 40 in
  let expected =
    [ 0; 2; 3; 4; 5; 6; 8; 10; 12; 14; 16; 18; 20; 22; 24; 26; 28; 30; 32; 34;
      36; 38; 40; 42; 44; 46; 48; 50; 52; 54; 56; 58; 60; 62; 64; 66; 68; 70;
      72; 74]
  in
  result = expected && List.length expected = 40

let%test "divide_along_axis: large xz triangle, increasing" =
  let matrix = Model.new_matrix 77 in
  OSeq.(1 -- 75) |> OSeq.iter (fun z ->
    OSeq.(1 -- z) |> OSeq.iter (fun x ->
      Model.fill' matrix (x,0,z);
    )
  );
  let result = divide_along_axis ~suppress_warning:() matrix (entire_model 77) `Z 40 in
  let expected =
    [ 0; 13; 18; 22; 26; 29; 32; 35; 37; 39; 41; 43; 45; 47; 49; 51; 53; 55; 57;
      58; 59; 60; 61; 62; 63; 64; 65; 66; 67; 68; 69; 70; 71; 72; 73; 74; 75 ]
  in
  (* Only 37 bots used -- not quite optimal *)
  result = expected && List.length expected = 37

let%test "divide_along_axis: subregion of small solid box" =
  let matrix = Model.new_matrix 8 in
  OSeq.(1 -- 6) |> OSeq.iter (fun x ->
    OSeq.(0 -- 2) |> OSeq.iter (fun y ->
      OSeq.(1 -- 6) |> OSeq.iter (fun z ->
        Model.fill' matrix (x,y,z);
      )
    )
  );
  let result = divide_along_axis matrix (region (coord 2 2 2) (coord 5 5 5)) `X 40 in
  eprintf "R %s" (result |> List.map ~f:string_of_int |> string_of_list);
  let expected = [ 2; 3; 4; 5 ] in
  result = expected
