open Core
open Types
open MicroLang
open MicroLangHelper
open TracerHelpers
open PrettyPrinters
open StrategyHelpers


let strategy_on_region (m : Model.matrix) (Region (r1, r2) as region) (layer_strategy : region -> microtrace) (ylayers : (int*int) list) =
  (* Layer-strategy promises to be at standard out-pos after running, i.e. directly above lln-corner of region *)
  (* TODO: Optimize the region itself *)
  ylayers
  |> List.fold ~init:[] ~f:(fun traces_r (from_y, to_y) ->
         (* Printf.printf "Running on layer %d--%d\n" from_y to_y; *)
         let layer = slice_region `Y region from_y to_y in
         let trace = layer_strategy layer in
         (* Printf.printf "Microcode for layer:\n\t%s\n\n" (string_of_microtrace trace); *)
         trace::traces_r
         (* (strategy_on_optimized_region m layer_strategy layer)::traces_r *)
       )
  |> List.rev
  |> Array.concat

let strategy (Matrix (r,_) as m : Model.matrix) (layer_strategy : region -> microtrace) =
  let layers = DivideLayers.layers m in
  let trace = strategy_on_region m (Model.entire_active_region m) layer_strategy layers in
  let back_down = trace_linear_move (ldiff_y (-(r-1))) (coord 0 (r-1) 0)
                  |> fst |> microtrace_of_singular_trace in
  Array.append trace back_down 
