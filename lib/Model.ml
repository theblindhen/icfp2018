open Core
open Types

type matrix = Matrix of int * Bitv.t

let new_matrix (r : int) =
  if r <= 0 || r > 250 then
    failwith "Invalid matrix size"
  else
    Matrix (r, Bitv.create (r * r * r) false)

let model_size (Matrix (r,_) : matrix) = r

let copy_matrix (Matrix (r, m)) =
  Matrix (r, Bitv.copy m)

let coord_to_idx (Matrix (r, _) : matrix) (Coord (x,y,z) : coord) =
  x * r * r + y * r + z

let coord_to_idx' (Matrix (r, _) : matrix) ((x,y,z) : (int * int * int)) =
  x * r * r + y * r + z

let idx_to_coord' (Matrix (r, _) : matrix) (i : int) =
  let z = i % r in
  let y = ((i - z) / r) % r in
  let x = (i - z - (r * y)) / (r * r) in
    (x, y, z)

let is_filled (m : matrix) (c : coord) =
  let Matrix (_, mat) = m in
  Bitv.get mat (coord_to_idx m c)

let is_filled' (m : matrix) ((x,y,z) : (int * int * int)) =
  let Matrix (_, mat) = m in
  Bitv.get mat (coord_to_idx' m (x,y,z))

let fill (m : matrix) (c : coord) =
  let Matrix (_, mat) = m in
  Bitv.set mat (coord_to_idx m c) true

let fill' (m : matrix) (pos : int * int * int) =
  let Matrix (_, mat) = m in
  Bitv.set mat (coord_to_idx' m pos) true

let void (m : matrix) (c : coord) =
  let Matrix (_, mat) = m in
  Bitv.set mat (coord_to_idx m c) false

let void' (m : matrix) (pos : int * int * int) =
  let Matrix (_, mat) = m in
  Bitv.set mat (coord_to_idx' m pos) false

let entire_active_region (Matrix (r,_)) = region (coord 0 0 0) (coord (r-2) (r-2) (r-2))

let count_voxels (Matrix (_, mat)) =
  Bitv.fold_left (fun acc bit -> if bit then acc+1 else acc) 0 mat

let count_voxels_in_region (Matrix (_, mat) as m) (Region (Coord (rx1,ry1,rz1), Coord (rx2,ry2,rz2))) =
  OSeq.(rx1 -- rx2) |> OSeq.map (fun x ->
    OSeq.(ry1 -- ry2) |> OSeq.map (fun y ->
      OSeq.(rz1 -- rz2) |> OSeq.map (fun z ->
        Bitv.get mat (coord_to_idx' m (x,y,z)) |> Bool.to_int
      )
      |> OSeq.sum
    )
    |> OSeq.sum
  )
  |> OSeq.sum

let load_model (file : string) =
  let ch = In_channel.create ~binary:true file in
  let r = match In_channel.input_byte ch with
    | None ->  failwith "No bytes in model file"
    | Some r -> r
  in
  let _get_bit_state = ref (8, 0) in
  let nth_bit x n = x land (1 lsl n) <> 0 in
  let get_bit () =
    let i,b = !_get_bit_state in
    if i > 7 then
      let b = match In_channel.input_byte ch with
        | None -> failwith "Not enough bytes in model file"
        | Some b -> b
      in
      _get_bit_state := (1, b);
      nth_bit b 0
    else
      (_get_bit_state := (i+1, b);
      nth_bit b i)
    in
  let m = new_matrix r in
  for x = 0 to r-1 do
    for y = 0 to r-1 do
      for z = 0 to r-1 do
        if get_bit () then
          fill' m (x,y,z)
        else
          ()
      done
    done
  done;
  In_channel.close ch;
  m

  (* TODO: Rem *)
let string_of_coord (Coord (dx, dy, dz)) =
  Printf.sprintf "(%d, %d, %d)" dx dy dz

let string_of_region (Region (r1, r2)) =
  Printf.sprintf "[[%s--%s]]" (string_of_coord r1) (string_of_coord r2)

let optimize_region (m : matrix) (Region (Coord (x1,y1,z1) as lln, Coord (x2,y2,z2)) as reg : region) =
  let extremal_plane u_to_plane u1 u2 =
    let rec extremal_plane' u =
      if u = u2 then u
      else
        if count_voxels_in_region m (u_to_plane u) > 0 then
          u
        else
          if u1 < u2
          then extremal_plane' (u+1)
          else extremal_plane' (u-1)
    in
    extremal_plane' u1
  in
  let minx = extremal_plane (fun x -> region (coord x y1 z1) (coord x y2 z2)) x1 x2 in
  let maxx = extremal_plane (fun x -> region (coord x y1 z1) (coord x y2 z2)) x2 x1 in
  if minx <= maxx then
    let miny = extremal_plane (fun y -> region (coord x1 y z1) (coord x2 y z2)) y1 y2 in
    let maxy = extremal_plane (fun y -> region (coord x1 y z1) (coord x2 y z2)) y2 y1 in
    let minz = extremal_plane (fun z -> region (coord x1 y1 z) (coord x2 y2 z)) z1 z2 in
    let maxz = extremal_plane (fun z -> region (coord x1 y1 z) (coord x2 y2 z)) z2 z1 in
    region (coord minx miny minz) (coord maxx maxy maxz)
  else (
    (* Empty region: return lower-left-near corner *)
    region lln lln
  )

let same_bitv b1 b2 =
  if Bitv.length b1 <> Bitv.length b2 then false else
  Bitv.foldi_left (fun acc i v1 -> acc && Bitv.get b2 i = v1) true b1

let same_matrix (m1 : matrix) (m2 : matrix) =
  let Matrix (r1, mat1) = m1 in
  let Matrix (r2, mat2) = m2 in
  r1 = r2 && same_bitv mat1 mat2

let print_layer (m : matrix) (y : int) =
  let Matrix (r,_) = m in
  for z = r-1 downto 0 do
    print_endline (String.concat (List.init r ~f:(fun x -> if is_filled' m (x,y,z) then "X" else ".")))
  done

let transform_matrix_coords f (Matrix (r, mat) as m : matrix) =
  let m_f = new_matrix r in
  Bitv.iteri (fun i b -> if b then
    idx_to_coord' m i |> f |> fill' m_f) mat;
  m_f

let%test "idx-to-coord'" =
  let m = new_matrix 20 in
  let c = (3, 17, 4) in
  coord_to_idx' m c |> idx_to_coord' m = c

let%test "transform matrix" =
  let m = new_matrix 10 in
  fill' m (1, 2, 3);
  let m_f = transform_matrix_coords (fun (x, y, z) -> (z, y, x)) m in
  is_filled' m_f (3, 2, 1)
