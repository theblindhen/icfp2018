open Core

let hello_world_string = "Hello, World"

let%test "starts with Hello" =
  String.is_prefix hello_world_string ~prefix:"Hello"
