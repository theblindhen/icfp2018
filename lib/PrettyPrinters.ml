open Core
open Types
open SimTypes

let string_of_list l =
  let content = List.fold ~init:"" ~f:(fun acc str -> if acc <> "" then acc ^ "; " ^ str else str) l in
  "[" ^ content ^ "]"

let string_of_long_int n =
  let rec put_primes sr = 
    if String.length sr <= 3 then
      sr
    else
      String.prefix sr 3 ^"'"^ (put_primes (String.drop_prefix sr 3))
  in
  string_of_int n
  |> String.rev
  |> put_primes
  |> String.rev

let string_of_coord (Coord (dx, dy, dz)) =
  Printf.sprintf "(%d, %d, %d)" dx dy dz

let string_of_region (Region (r1, r2)) =
  Printf.sprintf "[[%s--%s]]" (string_of_coord r1) (string_of_coord r2)

let string_of_diff (Diff (dx, dy, dz)) =
  Printf.sprintf "(%d, %d, %d)" dx dy dz

let string_of_botid (NanoLang.BotId id) = string_of_int id

let string_of_cmd (cmd : NanoLang.command) = match cmd with
  | NanoLang.Halt -> "Halt"
  | NanoLang.Wait -> "Wait"
  | NanoLang.Flip -> "Flip"
  | NanoLang.SMove lld -> 
    Printf.sprintf "SMove %s" (string_of_diff lld)
  | NanoLang.LMove (sld1, sld2) -> 
    Printf.sprintf "LMove %s %s" (string_of_diff sld1) (string_of_diff sld2)
  | NanoLang.Fission (nd, m) ->
    Printf.sprintf "Fission %s %d" (string_of_diff nd) m
  | NanoLang.Fill nd -> 
    Printf.sprintf "Fill %s" (string_of_diff nd)
  | NanoLang.Void nd ->
    Printf.sprintf "Void %s" (string_of_diff nd)
  | NanoLang.FusionP nd ->
    Printf.sprintf "FusionP %s" (string_of_diff nd)
  | NanoLang.FusionS nd ->
    Printf.sprintf "FusionS %s" (string_of_diff nd)
  | NanoLang.GFill (nd, fd) ->
    Printf.sprintf "GFill %s %s" (string_of_diff nd) (string_of_diff fd)
  | NanoLang.GVoid (nd, fd) ->
    Printf.sprintf "GVoid %s %s" (string_of_diff nd) (string_of_diff fd)

let string_of_nanocmds (cmds : (NanoLang.botid * NanoLang.command) list) = 
    cmds
    |> List.map ~f:(fun (botid, cmd) ->
        "("^ (string_of_botid botid) ^": "^ (string_of_cmd cmd) ^")")
    |> string_of_list

let string_of_nanotrace (nanotrace : NanoLang.nanotrace) : string =
  nanotrace
  |> List.map ~f:string_of_nanocmds
  |> string_of_list

let string_of_bot ({ id = BotId id; coord = pos; seeds = seeds } : nanobot) =
  Printf.sprintf "(id: %d, pos: %s, seeds: %s)"
    id (string_of_coord pos) (List.map ~f:string_of_botid seeds |> string_of_list)


let string_of_microcmd (cmd : MicroLang.microcmd) = match cmd with
  | MicroLang.Wait c -> string_of_coord c ^": Wait"
  | MicroLang.Flip c -> string_of_coord c ^": Flip"
  | MicroLang.SMove (c, lld) -> 
    Printf.sprintf "%s: SMove %s" (string_of_coord c) (string_of_diff lld)
  | MicroLang.LMove (c, sld1, sld2) -> 
    Printf.sprintf "%s: LMove %s %s" (string_of_coord c) (string_of_diff sld1) (string_of_diff sld2)
  | MicroLang.Fission (c, nd, m) ->
    Printf.sprintf "%s: Fission %s %d" (string_of_coord c) (string_of_diff nd) m
  | MicroLang.Fuse (c, nd, m) ->
    Printf.sprintf "%s: Fuse %s %d" (string_of_coord c) (string_of_diff nd) m
  | MicroLang.Fill (c, nd) -> 
    Printf.sprintf "%s: Fill %s" (string_of_coord c) (string_of_diff nd)
  | MicroLang.Void (c, nd) ->
    Printf.sprintf "%s: Void %s" (string_of_coord c) (string_of_diff nd)

let string_of_microcmdlist (cmds : MicroLang.microcmd list) : string =
  cmds
  |> List.map ~f:string_of_microcmd
  |> string_of_list

let string_of_microtrace (trace : MicroLang.microtrace) : string =
  trace
  |> Array.to_list
  |> List.map ~f:string_of_microcmdlist
  |> string_of_list
