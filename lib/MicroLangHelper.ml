open Core
open Types
open MicroLang

let cmd_coord (cmd : microcmd) : coord = match cmd with
  | Wait(c) -> c
  | Flip(c) -> c
  | SMove(c, _) -> c
  | LMove(c, _, _) -> c
  | Fission(c, _, _) -> c
  | Fuse(c, _, _) -> c
  | Fill(c, _) -> c
  | Void(c, _) -> c

let cmd_next_coords (cmd: microcmd) : coord list = match cmd with
  | Wait(c) -> [c]
  | Flip(c) -> [c]
  | SMove(c, nd) -> [c +: nd]
  | LMove(c, sld1, sld2) -> [c +: sld1 +: sld2]
  | Fission(c, nd, _) -> [c; c +: nd]
  | Fuse(c, _, _) -> [c]
  | Fill(c, _) -> [c]
  | Void(c, _) -> [c]

let shift_microcmd (diff : 'a gen_diff) = function
  | Wait (c) -> Wait (c +: diff)
  | Flip (c) -> Flip (c +: diff)
  | SMove (c, dt) -> SMove (c +: diff, dt)
  | LMove (c, dt1, dt2) -> LMove (c +: diff, dt1, dt2)
  | Fission (c, dt, m) -> Fission (c +: diff, dt, m)
  | Fuse (c, dt, m) -> Fuse (c +: diff, dt, m)
  | Fill (c, dt) -> Fill (c +: diff, dt)
  | Void (c, dt) -> Void (c +: diff, dt)

let shift_microtrace (diff : 'a gen_diff) (trace : microtrace) : microtrace =
    Array.map ~f:(List.map ~f:(shift_microcmd diff)) trace

let bots_before_cmdlist (trace : microcmd list) =
  List.map ~f:cmd_coord trace

let bots_after_cmdlist (trace : microcmd list) =
  List.map ~f:cmd_next_coords trace |> List.concat
  
let bots_before_trace (trace : microtrace) =
  if Array.is_empty trace
  then []
  else bots_before_cmdlist trace.(0)

let bots_after_trace (trace : microtrace) =
  if Array.is_empty trace
  then []
  else bots_after_cmdlist trace.(Array.length trace - 1)

let pad_with_wait (trace : microtrace) n =
  let waits = List.map (bots_after_trace trace) ~f:(fun c -> Wait c) in
  Array.append trace (Array.init n ~f:(fun _ -> waits))

let weave_two_traces_exn (tr_left : microtrace) (tr_right : microtrace) =
  (* Throws exception if either is empty *)
  if tr_left = [||] && tr_right = [||] then failwith "weaving with an empty trace is unsafe";
  let tr_left, tr_right =
    if Array.length tr_left < Array.length tr_right
    then tr_left, tr_right
    else tr_right, tr_left
  in
  let padded = pad_with_wait tr_left (Array.length tr_right - Array.length tr_left) in
  Array.map2_exn padded tr_right ~f:(fun cmd_left cmd_right -> cmd_left @ cmd_right)

(* Throws exception if any of the traces are empty *)
let weave_traces_exn = function
  | [] -> [||]
  | hd::tl -> List.fold ~init:hd tl ~f:weave_two_traces_exn

let add_waiting_bots (trace : microtrace) (bots : coord list) =
  let waits = List.map bots ~f:(fun c -> Wait c) in
  Array.map2_exn trace (Array.init (Array.length trace) ~f:(fun _ -> waits)) ~f:(@)

let%test "pad_with_wait" =
  let bot1, bot2 = coord 0 0 0, coord 1 1 1 in
  let tr1 = [| [ Flip bot1; Flip bot2 ] |] in
  pad_with_wait tr1 2 = [| [ Flip bot1; Flip bot2 ]; [ Wait bot1; Wait bot2 ]; [ Wait bot1; Wait bot2 ] |]

let%test "weave_two_traces" =
  let bot1, bot2 = coord 0 0 0, coord 1 1 1 in
  let tr1 = [| [ Wait bot1 ]; |] in
  let tr2 = [| [ Wait bot2 ]; [ Wait bot2 ] |] in
  weave_two_traces_exn tr1 tr2 = [| [ Wait bot1; Wait bot2 ]; [ Wait bot1; Wait bot2 ] |]

let%test "weave_traces1" =
  let bot1, bot2 = coord 0 0 0, coord 1 1 1 in
  let tr1 = [| [ Wait bot1 ]; |] in
  let tr2 = [| [ Wait bot2 ]; [ Wait bot2 ] |] in
  weave_traces_exn [tr1;tr2] = [| [ Wait bot1; Wait bot2 ]; [ Wait bot1; Wait bot2 ] |]

let%test "weave_traces" =
  let bot1, bot2, bot3 = coord 0 0 0, coord 1 1 1, coord 2 2 2 in
  let tr1 = [| [ Wait bot1 ]; |] in
  let tr2 = [| [ Wait bot2 ]; [ Wait bot2 ] |] in
  let tr3 = [| [ Wait bot3 ]; [ Wait bot3 ] |] in
  let tr = weave_traces_exn [tr1;tr2;tr3] in
  tr = [| [ Wait bot3; Wait bot1; Wait bot2 ]; [ Wait bot3; Wait bot1; Wait bot2  ] |]

let%test "add_waiting_bots" =
  let bot1, bot2, bot3 = coord 0 0 0, coord 1 1 1, coord 2 2 2 in
  let tr = [| [ Wait bot1 ]; [ Wait bot1 ] |] in
  add_waiting_bots tr [bot2;bot3] = [| [ Wait bot1; Wait bot2; Wait bot3 ]; [ Wait bot1; Wait bot2; Wait bot3  ] |]
