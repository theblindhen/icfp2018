open Core
open Types
open Model
open PrettyPrinters

let minimum_layer_height = 10

let filled_coords_in_layer (Matrix (r, _) as m) y =
  List.range 0 r
  |> List.concat_map ~f:(fun x ->
      List.range 0 r
      |> List.map ~f:(fun z -> coord x y z))
  |> List.filter ~f:(Model.is_filled m)

let coord_above (Matrix (r, _)) (Coord (x, y, z) : coord) =
  if y < r-1 then [coord x (y+1) z] else []

let in_or_below_layer layer_y (Coord (_, y, _)) = y <= layer_y

let ground_layer (Matrix (r, _) as m) (grounded: matrix) (ungrounded: matrix) y =
  let step delta =
    List.filter delta ~f:(fun coord ->
      match is_filled grounded coord with
      | true -> false
      | false -> 
          fill grounded coord; 
          void ungrounded coord; 
          true)
    |> List.concat_map ~f:(adjacent_coords ~r)
    |> List.filter ~f:(in_or_below_layer y)
    |> List.filter ~f:(is_filled m)
  in
  let rec iter delta =
    match step delta with
    | [] -> ()
    | next -> iter next
  in 
  let direct_grounding =
    List.concat_map (filled_coords_in_layer grounded (y-1)) ~f:(coord_above m)
    |> List.filter ~f:(is_filled m)
  in iter direct_grounding
    
let split_layer m ungrounded y =
  (* simple criterion to split layer when we hit something not grounded:
     count_voxels ungrounded > 0 *)
  List.exists 
    (filled_coords_in_layer m y)
    ~f:(fun (Coord (x, y, z)) -> 
      not (is_filled m (coord x (y-1) z)) || 
           is_filled ungrounded (coord x (y-1) z))

let rec divide_layers (Matrix (r, _) as m) grounded ungrounded y_from y_to =
  if y_to < r-2 then begin
    List.iter (filled_coords_in_layer m y_to) ~f:(fill ungrounded);
    ground_layer m grounded ungrounded y_to; 
    if (y_to - y_from) >= minimum_layer_height && split_layer m ungrounded y_to then
      (y_from, y_to-1)::divide_layers m grounded ungrounded y_to (y_to+1)
    else divide_layers m grounded ungrounded y_from (y_to+1)
  end else [(y_from, y_to)]

let layers (Matrix (r, _) as m) =
  let grounded = new_matrix r in
  let ungrounded = new_matrix r in
  filled_coords_in_layer m 0 |> 
  List.iter ~f:(fun c -> fill grounded c);
  divide_layers m grounded ungrounded 0 1

let%test "extend until hanging" =
  let m = new_matrix 50 in
  List.range 0 45 |> List.iter ~f:(fun y -> fill m (coord 0 y 0));
  fill m (coord 10 5 0);
  fill m (coord 10 15 0);
  List.range 1 11 |> List.iter ~f:(fun x -> fill m (coord x 6 0));
  List.range 1 11 |> List.iter ~f:(fun x -> fill m (coord x 16 0));
  layers m = [(0, 14); (15, 48)]

let%test "extend until hanging" =
  let m = new_matrix 50 in
  List.range 0 45 |> List.iter ~f:(fun y -> fill m (coord 0 y 0));
  List.range 1 11 |> List.iter ~f:(fun x -> fill m (coord x 16 0));
  List.range 1 11 |> List.iter ~f:(fun x -> fill m (coord x 17 0));
  layers m = [(0, 15); (16, 48)]
