open Core
open Types
open Sim
open SimTypes
open SimHelpers
open NanoLang
open MicroLang
open MicroLangHelper
open PrettyPrinters

module CoordMap = Map.Make(Coord)
module IntSet = Set.Make(Int)

type microstate = (botid * seeds) CoordMap.t

let init_microstate () : microstate =
  CoordMap.singleton (coord 0 0 0) (BotId 1, List.init 39 ~f:(fun i -> BotId (i+2)))

let move_bot (s : microstate) (src : coord) (dst : coord) bot =
  CoordMap.remove s src 
  |> CoordMap.add_exn ~key:dst ~data:bot

let find_bot (s : microstate) (c : coord) =
  match CoordMap.find s c with
  | Some x -> x
  | None ->
      Printf.printf "Failed to find a bot at coord %s\n%!" (string_of_coord c);
      CoordMap.to_alist s
      |> List.iter ~f:(fun (c, (BotId bid, _)) ->
        Printf.printf "Bot %d should be at %s\n%!" bid (string_of_coord c));
      raise (BadTrace "Bot not found")

let cmd_to_nanolang (s : microstate) (cmd : microcmd) = 
    let c = cmd_coord cmd in
    (* Printf.printf "Conv command %s\n" (string_of_microcmd cmd); *)
    let (botid, seeds) as bot = find_bot s c in
    match cmd with
    | Wait(_) -> (s, [(botid, NanoLang.Wait)])
    | Flip(_) -> (s, [(botid, Flip)])
    | SMove(c, lld) -> 
        (move_bot s c (c +: lld) bot, [(botid, SMove(lld))])
    | LMove(_, sld1, sld2) -> 
        (move_bot s c (c +: sld1 +: sld2) bot, [(botid, LMove(sld1, sld2))])
    | Fission(_, nd, m) -> 
        let (botid_s, seeds_p, seeds_s) = partion_seeds seeds m in
        let s' = 
          CoordMap.remove s c
          |> CoordMap.add_exn ~key:c ~data:(botid, seeds_p)
          |> CoordMap.add_exn ~key:(c +: nd) ~data:(botid_s, seeds_s) in
        (s', [(botid, Fission(nd, m))])
    | Fuse(c, nd, _) -> 
        let (botid_s, seeds_s) = find_bot s (c +: nd) in
        let s' = 
            CoordMap.remove s c
            |> Fn.flip CoordMap.remove (c +: nd)
            |> CoordMap.add_exn ~key:c ~data:(botid, botid_s :: seeds_s @ seeds) in
        (s', [(botid, FusionP(nd)); (botid_s, FusionS(invert_diff nd))])
    | Fill(_, nd) -> (s, [(botid, Fill(nd))])
    | Void(_, nd) -> (s, [(botid, Void(nd))])

let cmds_to_nanolang (s : microstate) (cmds : microcmd list) : microstate * (botid * command) list = 
    (* Printf.printf "Turn\n"; *)
    let (s', nano_cmds) = 
        List.fold cmds ~init:(s, []) ~f:(fun (acc_s, acc_cmds) cmd -> 
            let s', nano_cmds = cmd_to_nanolang acc_s cmd in
              (s', nano_cmds @ acc_cmds))
    in
    let rem_waits =
      let get_id (BotId id,_) = id in
      let commanded_botids = List.map ~f:get_id nano_cmds in
      let all_bots =
        CoordMap.data s
        |> List.map ~f:get_id
        |> IntSet.of_list
      in
      commanded_botids
      |> List.fold ~init:all_bots ~f:IntSet.remove
      |> IntSet.to_list
      |> List.map ~f:(fun id -> (BotId id, NanoLang.Wait))
    in
    let nano_cmds = rem_waits @ nano_cmds in
    (* Printf.printf "Micro %s\n" (string_of_microcmdlist cmds);
     * Printf.printf "Nano %s\n" (string_of_nanocmds nano_cmds); *)
    assert(List.length nano_cmds = CoordMap.length s);
    (s', nano_cmds)
  
let to_nanolang_seq (t : microtrace) : nanoseq = 
  let s = init_microstate () in
  Array.to_sequence_mutable t
  |> Sequence.folding_map ~init:s ~f:cmds_to_nanolang

let%test "compile fission, fuse" =
  let b1 = BotId 1 in
  let b2 = BotId 2 in
  let trace = [|[Fission(origin, ndiff_x 1, 5)]; [Fuse(origin, ndiff_x 1, 5)]|] in
  Sequence.to_list (to_nanolang_seq trace) = 
    [[(b1, Fission(ndiff_x 1, 5))]; [(b1, FusionP(ndiff_x 1)); (b2, FusionS(ndiff_x (-1)))]]

let%test "inverted-microlang inverts original" =
  let b1 = BotId 1 in
  let trace = [|[Fission(origin, ndiff_x 1, 5)]; [Fill(origin +: ndiff_x 1, ndiff_y 1); Fill(origin, ndiff_y 1)]|] in
  let inv_trace = invert_microtrace trace in
  let trace_nano = to_nanolang_seq (Array.append trace inv_trace) in
  let nano_trace = Sequence.append trace_nano (Sequence.return [(b1, Halt)]) in
  let nano_state = initial_state (Model.new_matrix 10) (Model.new_matrix 10) (flatten_nanotrace nano_trace) in
  run_sim nano_state |> ignore;
  Model.same_matrix nano_state.matrix (Model.new_matrix 10)
