open Core
open Sim
open SimTypes
open Types
open MicroLang
open MicroLangHelper

let optimize (Matrix (r,_) : Model.matrix) (old_trace : microtrace) =
  let new_trace_r = ref [] in
  let grounded_m = Model.new_matrix r in
  let ungrounded_m = Model.new_matrix r in
  let unground_c = ref 0 in
  let harmonics = ref Low in
  let is_grounded (Coord (_,y,_) as coord) =
    if y = 0 then true
    else
       adjacent_coords ~r:r coord
       |> List.exists ~f:(fun neighbor ->
              Model.is_filled grounded_m neighbor && (not (Model.is_filled ungrounded_m neighbor)))
  in
  let rec flood_grounded coord =
    adjacent_coords ~r:r coord
    |> List.filter ~f:(fun neighbor ->
           if Model.is_filled ungrounded_m neighbor && not (Model.is_filled grounded_m neighbor) then
             begin
             (* Printf.printf "Grounding stuff\n"; *)
             Model.fill grounded_m neighbor;
             assert (Model.is_filled ungrounded_m neighbor);
             Model.void ungrounded_m neighbor;
             unground_c := !unground_c - 1;
             true
             end
           else
             false)
    |> List.iter ~f:flood_grounded
  in
  let flip_trace = function
    | [] -> failwith "Gotta have bots?!"
    | hd::tl -> Flip hd :: (List.map tl ~f:(fun c -> Wait c))
  in
  Array.iteri old_trace ~f:(fun _ cmds ->
    (* Add all fills to the built map and ground what becomes grounded.
       We can simultaneously also count ungrounded new fills since if a later
       bot's fill causes a current bot's fill to become grounded, then this will
       be noticed and counted when considering that later bot.
     *)
    List.iter cmds ~f:(function
        | Fill (bot_pos, nd) ->
           let c_fill = bot_pos +: nd in
           if is_grounded c_fill then begin
             Model.fill grounded_m c_fill;
             flood_grounded c_fill
             end
           else begin
               (* Printf.printf "Something ungrounded\n"; *)
               Model.fill ungrounded_m c_fill;
               unground_c := !unground_c + 1
             end
        | Flip _ ->
           failwith "HighLowOptimizer currently doesn't support input traces with own flipping"
        | Void _ ->
            failwith "HighLowOptimizer: unsupported drawing command"
        | Wait _ | LMove _ | SMove _ | Fission _ |
          Fuse _ -> ()
      );
    match (!harmonics, !unground_c) with
    | (High, 0) -> begin
        (* Go low after the current move *)
        (* Printf.printf "Going low in round %d\n" round; *)
        new_trace_r := (flip_trace (bots_after_cmdlist cmds)) :: cmds :: !new_trace_r;
        harmonics := Low
        end
    | (Low, c) when c > 0 -> begin
        (* Go high before the current move *)
        (* Printf.printf "Going High in round %d\n" round; *)
        new_trace_r := cmds :: (flip_trace (bots_before_cmdlist cmds)) :: !new_trace_r;
        harmonics := High
        end
    | _ ->
        new_trace_r := cmds :: !new_trace_r
    );
  (* Go low before halting *)
  begin
  if !harmonics = High then
    let c = List.hd_exn !new_trace_r |> List.map ~f:cmd_next_coords |> List.concat |> List.hd_exn in
    new_trace_r := (flip_trace [c]) :: !new_trace_r
  end;
  !new_trace_r |> List.rev |> Array.of_list
