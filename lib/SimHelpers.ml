open Core
open Types
open Model
open NanoLang
open SimTypes

let sort_seeds (seeds: botid list) =
  List.sort ~compare:(fun (BotId b1) (BotId b2) -> Int.compare b1 b2) seeds

(** returns (seconary's bot id, primary's seeds, seconary's seeds) *)
let partion_seeds (seeds : botid list) (m : int) =
  let sorted_seeds = sort_seeds seeds in
  (List.hd_exn sorted_seeds, List.drop sorted_seeds (m+1), List.take sorted_seeds (m+1) |> List.tl_exn)
