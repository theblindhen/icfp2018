open Core
open Types
open Model

(* UNIT TESTS *)
let prob_dir = "../../../problemsL"
      
let%test "loading of models" =
  if false then begin
  let model_files = Sys.readdir prob_dir in
  Array.iter model_files ~f:(fun model_file ->
      if String.is_suffix ~suffix:".mdl" model_file then
        let _ = load_model (prob_dir ^"/"^ model_file) in
        ()
    );
  end;
  true

let %test "optimize_region" =
    let m = load_model (prob_dir ^"/"^ "LA001_tgt.mdl") in
    let Matrix (r,_) = m in
    printf "%s\n" (PrettyPrinters.string_of_region (optimize_region m (region (coord 0 0 0) (coord (r-1) (r-1) (r-1)))));
    true
