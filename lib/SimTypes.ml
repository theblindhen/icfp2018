open Core
open Types
open Model
open NanoLang

exception BadTrace of string

type seeds = botid list
type nanobot = {
  id: botid;
  mutable coord: coord;
  mutable seeds: seeds;
}

type harmonics = Low | High
type bots = nanobot list

type cmd_group = 
  | Singleton of command * nanobot
  | FusionPair of {
    primary: ndiff * nanobot;
    secondary: ndiff * nanobot
  }
  | GroupRegion of {
    region: region;
    operation: [`Fill|`Void];
    bots: nanobot list;
  }

type energy = {
  mutable e_time: int;
  mutable e_bots: int;
  mutable e_fill: int;
  mutable e_move: int;
  mutable e_other: int;
}

type state = {
    goal: matrix;
    energy: energy;
    mutable harmonics: harmonics;
    matrix: matrix;
    mutable bots: bots;
    mutable trace: command Sequence.t;
}
