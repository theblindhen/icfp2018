open Core
open Types
open MicroLang

(* Helpers and combinators for single-bot movement traces. These return microcmd
   lists, i.e. a list of single microcmd for that bot.
   Use microtrace_of_singular_trace to convert to microtrace.
 *)

let (>>:) ((cmds, cur_pos) : ('a list * coord)) (f : coord -> 'a list * coord) =
  let cmds', new_pos = f cur_pos in
  (cmds @ cmds', new_pos)

let trace_smove (diff : lldiff) (cur_pos : coord) =
  ([SMove (cur_pos,diff)], cur_pos +: diff)

let trace_linear_move (diff : ldiff) (cur_pos : coord) =
  let target_pos = cur_pos +: diff in
  let rec go_on cur_pos =
    let rem = clen(target_pos -: cur_pos) in
    if rem > 15 then
      ([], cur_pos)
      >>: trace_smove (lldiff_of_diff (15 *: direction_of_diff diff))
      >>: go_on
    else if rem > 0 then
      ([], cur_pos)
      >>: trace_smove (lldiff_of_diff (rem *: direction_of_diff diff))
    else
      ([], cur_pos)
  in
  ([], cur_pos) >>: go_on 

let trace_diff_move_xzy (Diff (dx,dy,dz) : diff) (cur_pos : coord) =
   ([], cur_pos)
   >>: trace_linear_move (ldiff_x dx)
   >>: trace_linear_move (ldiff_z dz)
   >>: trace_linear_move (ldiff_y dy)

let trace_diff_move_zxy (Diff (dx,dy,dz) : diff) (cur_pos : coord) =
   ([], cur_pos)
   >>: trace_linear_move (ldiff_z dz)
   >>: trace_linear_move (ldiff_x dx)
   >>: trace_linear_move (ldiff_y dy)

let trace_fill (fill_diff : ndiff) cur_pos = ([ Fill (cur_pos, fill_diff) ], cur_pos)

let trace_cond_fill m (fill_diff : ndiff) cur_pos =
  ((if Model.is_filled m (cur_pos +: fill_diff) then [ Fill (cur_pos, fill_diff) ] else []), cur_pos)

let trace_flip cur_pos =
  ([ Flip (cur_pos) ], cur_pos)

let microtrace_of_singular_trace trace =
  trace
  |> List.map ~f:(fun c -> [c])
  |> Array.of_list

(* The standard out-pos after a strategy *)
let region_pos_out (Region (Coord (rx1,_,rz1), Coord (_,ry2,_))) = coord rx1 (ry2+1) rz1
