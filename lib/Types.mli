open Core

type coord = private Coord of int * int * int

module Coord : sig
  type t = coord
  include Hashable.S with type t := t
  include Comparable.S with type t := t
  include Sexpable.S with type t := t
end

type -'a gen_diff = private Diff of int * int * int

type diff = [`any] gen_diff

type ldiff = [`l|`any] gen_diff
type lldiff = [`ll|`l|`f|`any] gen_diff
type sldiff = [`sl|`ll|`l|`f|`any] gen_diff
type ndiff = [`n|`f|`any] gen_diff
type fdiff = [`f|`any] gen_diff

type region = private Region of coord * coord

val coord : int -> int -> int -> coord

val origin : coord

val string_of_coord : coord -> string

(* Return the up to six adjacent coordinates of a coordinate, given a map size.
 *)
val adjacent_coords : r:int -> coord -> coord list

val (-:) : coord -> coord -> diff

val (+:) : coord -> 'a gen_diff -> coord

val ( *:) : int -> 'a gen_diff -> diff

val invert_diff : 'a gen_diff -> 'a gen_diff
val transform_coord : (int * int * int -> int * int * int) -> coord -> coord
val transform_diff : (int * int * int -> int * int * int) -> 'a gen_diff -> 'a gen_diff

val ndiff_x : int -> ndiff
val ndiff_y : int -> ndiff
val ndiff_z : int -> ndiff

val ndiff_xy : int -> int -> ndiff
val ndiff_xz : int -> int -> ndiff
val ndiff_yz : int -> int -> ndiff

val sldiff_x : int -> sldiff
val sldiff_y : int -> sldiff
val sldiff_z : int -> sldiff

val lldiff_x : int -> lldiff
val lldiff_y : int -> lldiff
val lldiff_z : int -> lldiff

val ldiff_x : int -> ldiff
val ldiff_y : int -> ldiff
val ldiff_z : int -> ldiff

val mlen : 'a gen_diff -> int
val clen : 'a gen_diff -> int

val is_ldiff  : 'a gen_diff -> bool
val is_ndiff : 'a gen_diff -> bool
val is_sldiff : 'a gen_diff -> bool
val is_lldiff : 'a gen_diff -> bool

val ldiff_of_diff : 'a gen_diff -> ldiff
val lldiff_of_diff : diff -> lldiff
val sldiff_of_diff : diff -> sldiff
val ndiff_of_diff : 'a gen_diff -> ndiff
val lldiff_of_ldiff : ldiff -> lldiff
val sldiff_of_ldiff : ldiff -> sldiff
val sldiff_of_lldiff : lldiff -> sldiff

val diff_of_triple : (int * int * int) -> diff
val ldiff_of_triple : (int * int * int) -> ldiff
val lldiff_of_triple : (int * int * int) -> lldiff
val sldiff_of_triple : (int * int * int) -> sldiff
val ndiff_of_triple : (int * int * int) -> ndiff
val fdiff_of_triple : (int * int * int) -> fdiff

val direction_of_diff : 'a gen_diff -> 'a gen_diff

val region : coord -> coord -> region
val region_dim : region -> int
val in_region: region -> coord -> bool
val in_region': region -> int -> int -> int -> bool
val region_size : region -> (int * int * int)
val coords_of_region : region -> coord OSeq.t

val opposite_corner: region -> coord -> coord

val slice_region: [< `X|`Y|`Z] -> region -> int -> int -> region
